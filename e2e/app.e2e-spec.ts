import { CrruiseTopicPage } from './app.po';

describe('crruise-topic App', () => {
  let page: CrruiseTopicPage;

  beforeEach(() => {
    page = new CrruiseTopicPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
