export class Search {
  constructor(
    public destination?: string,
    public month?: string,
    public company?: string,
    public port?: string
  ) {  }
}