import { Component, OnInit } from '@angular/core';
// PROVIDERS
import { PropertyService } from './../providers/property-service-mock';


@Component({
  selector: 'app-destinations-head',
  templateUrl: './destinations-head.component.html',
  styleUrls: ['./destinations-head.component.css']
})
export class DestinationsHeadComponent implements OnInit {

	destination_pin:any;
	loader:boolean = false;
	constructor(private _propertyService: PropertyService)  { 
		this.loader = true;
		this._propertyService.getHeaderPinDestinations().then(response => {
		this.destination_pin = response;
		this.loader = false;
	});
	}


  ngOnInit() {
  }

}
