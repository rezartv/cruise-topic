import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'prettyprint'
})
export class PrettyPrintPipe implements PipeTransform {
	/*{{myString | prettyprint}}*/
	transform(value: string, args: string[]): any {
		if (!value) return value;

		return '<pre>' + JSON.stringify(value, null, 2)
			.replace(' ', '&nbsp;')
			.replace('\n', '<br/>') + '</pre>';
	}
}


@Pipe({
	name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

    transform(value:any) {
        if (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
        return value;
    }

}