import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-inner-header-center',
  templateUrl: './inner-header-center.component.html',
  styleUrls: ['./inner-header-center.component.css']
})
export class InnerHeaderCenterComponent{
	
@Input() page_title: string="no title";

}
