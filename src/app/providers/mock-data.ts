export let SLIDES = [
    { caption: 'Mr. Nice', src: "./../../../assets/img/header1.jpg" },
    { caption: 'Narco', src: "./../../../assets/img/header2.jpg" },
    { caption: 'Bombasto', src: "./../../../assets/img/header3.jpg" },
    { caption: 'Celeritas', src: "./../../../assets/img/header4.jpg" }
];

export let OFFERS = {
    destination : {
        id: 1,
        type: "destination",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        price: "$475,000",
        title: "Destination",
        pic: "./../../../assets/img/oferte.jpg",
        thumb: "./../../../assets/img/oferte.jpg",
        offers: [
            {
                id: 1,
                title: "ANTILLE DA SCOPRIRE",
                description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                duration: "8 giorni 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "320€",
                link: "#",
                company_name: "Celebrity crules",
                company_logo: "./../../../assets/img/kompani/1.jpg" 
            },
            {
                id: 1,
                title: "Cuba Jamaica e Honduras",
                description: "Cuba, Giamaica, Isole Cayman, Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "340€",
                link: "#",
                company_name: "Costa",
                company_logo: "./../../../assets/img/kompani/2.jpg"                
            },
            {
                id: 1,
                title: "L'Havana e le altre perle dei Caraibi",
                description: "Cuba, Belize, Honduras e Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "360€",
                link: "#",
                company_name: "MSC Croiciere",
                company_logo: "./../../../assets/img/kompani/3.jpg"                
            },
            {
                id: 1,
                title: "Sole d'inverno",
                description: "Spagna Francia e Baleari",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: '380€',
                link: "#",
                company_name: "Princess Cruises",
                company_logo: "./../../../assets/img/kompani/4.jpg"                
            }
        ]
    },
    company: {
        id: 2,
        type: "company",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        price: "$475,000",
        title: "Company",
        pic: "https://s3-us-west-1.amazonaws.com/sfdc-demo/realty/house08wide.jpg",
        thumb: "https://s3-us-west-1.amazonaws.com/sfdc-demo/realty/house08sq.jpg",
        offers: [
            {
                id: 1,
                title: "Sole d'inverno",
                description: "Spagna Francia e Baleari",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: '$1,200,000',
                company_name: "Princess Cruises",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 2,
                title: "Cuba Jamaica e Honduras",
                description: "Cuba, Giamaica, Isole Cayman, Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "$1,200,000",
                company_name: "Costa",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 3,
                title: "ANTILLE DA SCOPRIRE",
                description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                duration: "8 giorni 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "320€",
                company_name: "Celebrity crules",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 4,
                title: "L'Havana e le altre perle dei Caraibi",
                description: "Cuba, Belize, Honduras e Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "$1,200,000",
                company_name: "MSC Croiciere",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            }
            
        ]
    },
    sezonal: {
        id: 3,
        type: "sezonal",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        price: "$475,000",
        title: "Sezonale",
        pic: "./../../../assets/img/ofert1.jpg",
        thumb: "./../../../assets/img/ofert1.jpg",
        offers: [
            {
                id: 1,
                title: "L'Havana e le altre perle dei Caraibi",
                description: "Cuba, Belize, Honduras e Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "$1,200",
                company_name: "MSC Croiciere",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 2,
                title: "Sole d'inverno",
                description: "Spagna Francia e Baleari",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: '$1,200,000',
                company_name: "Princess Cruises",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 3,
                title: "ANTILLE DA SCOPRIRE",
                description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                duration: "8 giorni 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "420€",
                company_name: "Celebrity crules",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 4,
                title: "Cuba Jamaica e Honduras",
                description: "Cuba, Giamaica, Isole Cayman, Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "$500",
                company_name: "Costa",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            }
        ]
    },
    port: {
        id: 4,
        type: "port",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        price: "$475,000",
        title: "Porto",
        pic: "./../../../assets/img/o4.png",
        thumb: "./../../../assets/img/o4.png",
        offers: [
            {
                id: 1,
                title: "Cuba Jamaica e Honduras",
                description: "Cuba, Giamaica, Isole Cayman, Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "$1,200,000",
                company_name: "Costa",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 2,
                title: "L'Havana e le altre perle dei Caraibi",
                description: "Cuba, Belize, Honduras e Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "$1,200,000",
                company_name: "MSC Croiciere",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 3,
                title: "Sole d'inverno",
                description: "Spagna Francia e Baleari",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: '$1,200,000',
                company_name: "Princess Cruises",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            },
            {
                id: 4,
                title: "ANTILLE DA SCOPRIRE",
                description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                duration: "8 giorni 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "320€",
                company_name: "Celebrity crules",
                company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"                
            }
        ]
    }
};

export let COMPANY_LIST = [
    {
        id: 1,
        title: "destination",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/1.jpg"
    },
    {
        id: 2,
        title: "company",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/2.jpg"
    },
    {
        id: 3,
        title: "sezonal",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/3.jpg"
    },
    {
        id: 4,
        title: "port",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/4.jpg"
    },
    {
        id: 5,
        title: "destination",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/5.png"
    },
    {
        id: 6,
        title: "company",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/6.jpg"
    },
    {
        id: 7,
        title: "sezonal",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/7.jpg"
    },
    {
        id: 0,
        title: "port",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/8.jpg"
    },
    {
        id: 8,
        title: "port",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/kompani/9.png"
    }
];

export let COMPANY = [
    {
        id: 1,
        title: "Costa",
        description: "Già dalla metà del 1800 la famiglia Costa aveva iniziato la sua attività con una flotta di navi merci che trasportavano tessuti e olio d'oliva nel Mediterraneo.",
        thumb: "./../../../assets/img/oferte.jpg",
        logo: "./../../../assets/img/kompani/2.jpg",
        location: {
            name: "Bari",
            lat: "",
            lng: ""
        },
        offers: [
            {
                id: 1,
                title: "ANTILLE DA SCOPRIRE",
                description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                duration: "8 giorni 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "320€",
                link: "#",
                company_name: "Costa",
                company_logo: "./../../../assets/img/kompani/2.jpg" 
            },
            {
                id: 2,
                title: "Cuba Jamaica e Honduras",
                description: "Cuba, Giamaica, Isole Cayman, Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "340€",
                link: "#",
                company_name: "Costa",
                company_logo: "./../../../assets/img/kompani/2.jpg"                
            },
            {
                id: 3,
                title: "L'Havana e le altre perle dei Caraibi",
                description: "Cuba, Belize, Honduras e Messico.",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: "360€",
                link: "#",
                company_name: "Costa",
                company_logo: "./../../../assets/img/kompani/2.jpg"                
            },
            {
                id: 4,
                title: "Sole d'inverno",
                description: "Spagna Francia e Baleari",
                duration: "8 Giorni, 7 notti",
                departure: "Genova",
                dates: "16-17-18",
                price: '380€',
                link: "#",
                company_name: "Costa",
                company_logo: "./../../../assets/img/kompani/2.jpg"                
            }
        ]
    },
    {
            id: 2,
            title: "MSC Crociere",
            description: "MSC Crociere e stata creata da Gianluigi Aponte, proprietario di Mediterranean Shipping Company ,nel 1987, incorporando la flotta Lauro precedentemente acquista. ",
            thumb: "./../../../assets/img/oferte.jpg",
            logo: "./../../../assets/img/kompani/3.jpg",
            location: {
                name: "Bari",
                lat: "",
                lng: ""
            },
            offers: [
                {
                    id: 1,
                    title: "ANTILLE DA SCOPRIRE",
                    description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                    duration: "8 giorni 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "320€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/3.jpg" 
                },
                {
                    id: 2,
                    title: "Cuba Jamaica e Honduras",
                    description: "Cuba, Giamaica, Isole Cayman, Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "340€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/3.jpg"                
                },
                {
                    id: 3,
                    title: "L'Havana e le altre perle dei Caraibi",
                    description: "Cuba, Belize, Honduras e Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "360€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/3.jpg"                
                },
                {
                    id: 4,
                    title: "Sole d'inverno",
                    description: "Spagna Francia e Baleari",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: '380€',
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/3.jpg"                
                }
            ]
    },
    {
            id: 3,
            title: "Royal Caribbean",
            description: "Royal Caribbean è stata fondata nel 1970 e ad oggi conta flotta di 21 navi in servizio, che nel tempo hanno rivoluzionato il concetto di crociera.",
            thumb: "./../../../assets/img/oferte.jpg",
            logo: "./../../../assets/img/kompani/5.jpg",
            location: {
                name: "Bari",
                lat: "",
                lng: ""
            },
            offers: [
                {
                    id: 1,
                    title: "ANTILLE DA SCOPRIRE",
                    description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                    duration: "8 giorni 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "320€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg" 
                },
                {
                    id: 2,
                    title: "Cuba Jamaica e Honduras",
                    description: "Cuba, Giamaica, Isole Cayman, Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "340€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 3,
                    title: "L'Havana e le altre perle dei Caraibi",
                    description: "Cuba, Belize, Honduras e Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "360€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 4,
                    title: "Sole d'inverno",
                    description: "Spagna Francia e Baleari",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: '380€',
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                }
            ]
    },
    {
            id: 4,
            title: "Norwegian Cruise Line",
            description: "In realta tutto nasce con un altro nome, Norwegian Carebbean dal suo fondatore la società norvegese Klosters Rederi di Oslo.",
            thumb: "./../../../assets/img/oferte.jpg",
            logo: "./../../../assets/img/kompani/5.jpg",
            location: {
                name: "Bari",
                lat: "",
                lng: ""
            },
            offers: [
                {
                    id: 1,
                    title: "ANTILLE DA SCOPRIRE",
                    description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                    duration: "8 giorni 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "320€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg" 
                },
                {
                    id: 2,
                    title: "Cuba Jamaica e Honduras",
                    description: "Cuba, Giamaica, Isole Cayman, Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "340€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 3,
                    title: "L'Havana e le altre perle dei Caraibi",
                    description: "Cuba, Belize, Honduras e Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "360€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 4,
                    title: "Sole d'inverno",
                    description: "Spagna Francia e Baleari",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: '380€',
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                }
            ]
    },
    {
            id: 5,
            title: "Celebrity Cruises",
            description: "In realta tutto nasce con un altro nome, Norwegian Carebbean dal suo fondatore la società norvegese Klosters Rederi di Oslo.",
            thumb: "./../../../assets/img/oferte.jpg",
            logo: "./../../../assets/img/kompani/5.jpg",
            location: {
                name: "Bari",
                lat: "",
                lng: ""
            },
            offers: [
                {
                    id: 1,
                    title: "ANTILLE DA SCOPRIRE",
                    description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                    duration: "8 giorni 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "320€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg" 
                },
                {
                    id: 2,
                    title: "Cuba Jamaica e Honduras",
                    description: "Cuba, Giamaica, Isole Cayman, Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "340€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 3,
                    title: "L'Havana e le altre perle dei Caraibi",
                    description: "Cuba, Belize, Honduras e Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "360€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 4,
                    title: "Sole d'inverno",
                    description: "Spagna Francia e Baleari",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: '380€',
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                }
            ]
    },
    {
            id: 6,
            title: "Princess Cruises",
            description: "In realta tutto nasce con un altro nome, Norwegian Carebbean dal suo fondatore la società norvegese Klosters Rederi di Oslo.",
            thumb: "./../../../assets/img/oferte.jpg",
            logo: "./../../../assets/img/kompani/5.jpg",
            location: {
                name: "Bari",
                lat: "",
                lng: ""
            },
            offers: [
                {
                    id: 1,
                    title: "ANTILLE DA SCOPRIRE",
                    description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                    duration: "8 giorni 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "320€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg" 
                },
                {
                    id: 2,
                    title: "Cuba Jamaica e Honduras",
                    description: "Cuba, Giamaica, Isole Cayman, Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "340€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 3,
                    title: "L'Havana e le altre perle dei Caraibi",
                    description: "Cuba, Belize, Honduras e Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "360€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 4,
                    title: "Sole d'inverno",
                    description: "Spagna Francia e Baleari",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: '380€',
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                }
            ]
    },
    {
            id: 7,
            title: "Royal Caribbean",
            description: "In realta tutto nasce con un altro nome, Norwegian Carebbean dal suo fondatore la società norvegese Klosters Rederi di Oslo.",
            thumb: "./../../../assets/img/oferte.jpg",
            logo: "./../../../assets/img/kompani/5.jpg",
            location: {
                name: "Bari",
                lat: "",
                lng: ""
            },
            offers: [
                {
                    id: 1,
                    title: "ANTILLE DA SCOPRIRE",
                    description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                    duration: "8 giorni 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "320€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg" 
                },
                {
                    id: 2,
                    title: "Cuba Jamaica e Honduras",
                    description: "Cuba, Giamaica, Isole Cayman, Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "340€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 3,
                    title: "L'Havana e le altre perle dei Caraibi",
                    description: "Cuba, Belize, Honduras e Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "360€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 4,
                    title: "Sole d'inverno",
                    description: "Spagna Francia e Baleari",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: '380€',
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                }
            ]
    },
    {
            id: 8,
            title: "Holland America",
            description: "In realta tutto nasce con un altro nome, Norwegian Carebbean dal suo fondatore la società norvegese Klosters Rederi di Oslo.",
            thumb: "./../../../assets/img/oferte.jpg",
            logo: "./../../../assets/img/kompani/1.jpg",
            location: {
                name: "Bari",
                lat: "",
                lng: ""
            },
            offers: [
                {
                    id: 1,
                    title: "ANTILLE DA SCOPRIRE",
                    description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                    duration: "8 giorni 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "320€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg" 
                },
                {
                    id: 2,
                    title: "Cuba Jamaica e Honduras",
                    description: "Cuba, Giamaica, Isole Cayman, Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "340€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 3,
                    title: "L'Havana e le altre perle dei Caraibi",
                    description: "Cuba, Belize, Honduras e Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "360€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 4,
                    title: "Sole d'inverno",
                    description: "Spagna Francia e Baleari",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: '380€',
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                }
            ]
    },
    {
            id: 9,
            title: "Carnival",
            description: "In realta tutto nasce con un altro nome, Norwegian Carebbean dal suo fondatore la società norvegese Klosters Rederi di Oslo.",
            thumb: "./../../../assets/img/oferte.jpg",
            logo: "./../../../assets/img/kompani/1.jpg",
            location: {
                name: "Bari",
                lat: "",
                lng: ""
            },
            offers: [
                {
                    id: 1,
                    title: "ANTILLE DA SCOPRIRE",
                    description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
                    duration: "8 giorni 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "320€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg" 
                },
                {
                    id: 2,
                    title: "Cuba Jamaica e Honduras",
                    description: "Cuba, Giamaica, Isole Cayman, Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "340€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 3,
                    title: "L'Havana e le altre perle dei Caraibi",
                    description: "Cuba, Belize, Honduras e Messico.",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: "360€",
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                },
                {
                    id: 4,
                    title: "Sole d'inverno",
                    description: "Spagna Francia e Baleari",
                    duration: "8 Giorni, 7 notti",
                    departure: "Genova",
                    dates: "16-17-18",
                    price: '380€',
                    link: "#",
                    company_name: "Costa",
                    company_logo: "./../../../assets/img/kompani/1.jpg"                
                }
            ]
    }
];

export let ARTICLE_LIST = [
    {
        id: 1,
        title: "Lorem ipsum article title",
        summary: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/greece.jpg",
        author: "Author Name",
        date: "November 1, 2016"
    },
    {
        id: 2,
        title: "Lorem ipsum article title",
        summary: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
        thumb: "./../../../assets/img/o3.png",
        author: "Author Name",
        date: "November 1, 2016"
    }
];

export let REVIEW = {
    id: 1,
    title: "Lorem ipsum article title",
    summary: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
    thumb: "./../../../assets/img/shipi.jpg",
    logo: "./../../../assets/img/kompani/1.jpg",
    rate: "4.3",
    author: "Author Name",
    date: "November 1, 2016"
};

export let OFFER_CATEGORY=[
    {
        id:1,
        thumb:"./../../../assets/img/o4.png",
        title:" compagnie",
        link:  '/offerte-crociere/compagnia'
    },
    {
        id:2,
        thumb:"./../../../assets/img/o2.png",
        title:"LAST  MINUTE",
        link:'/offerte-crociere-lastminute'
    },
    {
        id:3,
        thumb:"./../../../assets/img/o3.png",
        title:"DESTINAZIONE",
        link:'/offerte-crociere/destinazione'
    },
    {
        id:4,
        thumb:"./../../../assets/img/o1.png",
        title:"SESONALE",
        link:'/offerte-crociere/speciale'
    }
];

export let LINK_BAR =[
    {
        id:1,
        title:"Le destinazioni",
        i_class:"fa-map-marker",
        list : [
          { id: 21, name: 'Mr. Nice', link:'#'},
          { id: 22, name: 'Narco', link:'#' },
          { id: 23, name: 'Bombasto', link:'#' },
          { id: 24, name: 'Celeritas', link:'#' },
          { id: 25, name: 'Magneta', link:'#' },
          { id: 26, name: 'RubberMan', link:'#' },
          { id: 27, name: 'Dynama', link:'#' },
          { id: 28, name: 'Dr IQ', link:'#' },
          { id: 29, name: 'Magma', link:'#' },
          { id: 30, name: 'Tornado', link:'#' }
        ]
    },
    {
        id:2,
        title:"Le compagnie",
        i_class:"fa-building",
        list : [
          { id: 211, name: 'Mr. Nice',  link:'#'},
          { id: 212, name: 'Narco', link:'#' },
          { id: 213, name: 'Bombasto', link:'#' },
          { id: 214, name: 'Celeritas', link:'#' },
          { id: 215, name: 'Magneta',  link:'#'},
          { id: 216, name: 'RubberMan', link:'#' },
          { id: 217, name: 'Dynama',  link:'#'},
          { id: 218, name: 'Dr IQ' , link:'#'},
          { id: 219, name: 'Magma' , link:'#'},
          { id: 310, name: 'Tornado' , link:'#'}
        ]
    },
    {
        id:3,
        title:"Le navi",
        i_class:"fa-ship",
        list : [
          { id: 221, name: 'Mr. Nice' , link:'#'},
          { id: 222, name: 'Narco' , link:'#'},
          { id: 223, name: 'Bombasto' , link:'#'},
          { id: 224, name: 'Celeritas' , link:'#'},
          { id: 225, name: 'Magneta' , link:'#'},
          { id: 226, name: 'RubberMan', link:'#' },
          { id: 227, name: 'Dynama', link:'#' },
          { id: 228, name: 'Dr IQ', link:'#' },
          { id: 229, name: 'Magma', link:'#' },
          { id: 320, name: 'Tornado', link:'#' }
        ]
    },
    {
        id:4,
        title:"Last minute",
        i_class:"fa-clock-o",
        list : [
          { id: 231, name: 'Mr. Nice', link:'#' },
          { id: 232, name: 'Narco', link:'#' },
          { id: 233, name: 'Bombasto', link:'#' },
          { id: 234, name: 'Celeritas', link:'#' },
          { id: 235, name: 'Magneta', link:'#' },
          { id: 236, name: 'RubberMan', link:'#' },
          { id: 237, name: 'Dynama', link:'#' },
          { id: 238, name: 'Dr IQ' , link:'#'},
          { id: 239, name: 'Magma' , link:'#'},
          { id: 330, name: 'Tornado' , link:'#'}
        ]
    }
];

export let SINGLE_SEARCH_OFFER =[
    {
            id:1,
            prices:{
                details:{                   
                    destination:"cuba",
                    destination_description:"Cuba, Belize, Honduras, Messico",
                    ship:"MSC    ARMONIA ",
                    port:"LA  HABANA",
                    company_thumb:"./../../../assets/img/kompani/3.jpg",
                    rating_thumb:"./../../../assets/img/rate4.png",
                    destination_thumb: "./../../../assets/img/ofert1.jpg",
                    wishlist_id:"1",
                    checked:"checked"
                },

                cabina_interna_tooltip:"./../../../assets/img/kabin.jpg",
                cabina_esterna_tooltip:"./../../../assets/img/kabin1.jpg",
                cabina_balcone_tooltip:"./../../../assets/img/kabin2.jpg",
                suite_tooltip:"./../../../assets/img/kabin3.jpg",

            },

            departure_date:[
                {
                    date:"13 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"/offerte-crociere"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"14 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"15 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"16 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                // {
                //     date:"17 - 11  - 16",
                //     cabina_interna:{
                //         price:"630",
                //         link:"#"
                //     },
                //     cabina_esterna:{
                //         price:"300",
                //         link:"#"
                //     },
                //     cabina_balcone:{
                //         price:"630",
                //         link:"#"
                //     },
                //     suite:{
                //         price:"603",
                //         link:"#"
                //     }          
                // },
                // {
                //     date:"18 - 11  - 16",
                //     cabina_interna:{
                //         price:"630",
                //         link:"#"
                //     },
                //     cabina_esterna:{
                //         price:"300",
                //         link:"#"
                //     },
                //     cabina_balcone:{
                //         price:"630",
                //         link:"#"
                //     },
                //     suite:{
                //         price:"603",
                //         link:"#"
                //     }          
                // },
                //  {
                //     date:"19 - 11  - 16",
                //     cabina_interna:{
                //         price:"630",
                //         link:"#"
                //     },
                //     cabina_esterna:{
                //         price:"300",
                //         link:"#"
                //     },
                //     cabina_balcone:{
                //         price:"630",
                //         link:"#"
                //     },
                //     suite:{
                //         price:"603",
                //         link:"#"
                //     }          
                // },
                //  {
                //     date:"20 - 11  - 16",
                //     cabina_interna:{
                //         price:"630",
                //         link:"#"
                //     },
                //     cabina_esterna:{
                //         price:"300",
                //         link:"#"
                //     },
                //     cabina_balcone:{
                //         price:"630",
                //         link:"#"
                //     },
                //     suite:{
                //         price:"603",
                //         link:"#"
                //     }          
                // }

            ],

            itinerary:[
                {
                  day_nr:1,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:2,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"grey",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:3,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:4,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"grey",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:5,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                }
            ],
            gallery:[
                {   picture:"./../../../assets/img/gall1.jpg",
                    title:"first image"
                },
                {   picture:"./../../../assets/img/gall2.jpg",
                    title:"second image"
                },
                {   picture:"./../../../assets/img/kabin1.jpg",
                    title:"third image"
                },
                {   picture:"./../../../assets/img/kabin2.jpg",
                    title:"fourth image"
                },
            ],          
            review:[
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                },
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                },
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                }
            ] ,
            read_more_link:"#"
    },
    {
            id:2,
            prices:{
                details:{                   
                    destination:"Meraviglioso Mediterraneo",
                    destination_description:"Spagna, Francia, Malta ",
                    ship:"Msc Meraviglia",
                    port:"MESSINA",
                    company_thumb:"./../../../assets/img/kompani/1.jpg",
                    rating_thumb:"./../../../assets/img/rate4.png",
                    destination_thumb: "./../../../assets/img/o4.png",
                    wishlist_id:"2",
                    checked:""
                },

                cabina_interna_tooltip:"./../../../assets/img/kabin.jpg",
                cabina_esterna_tooltip:"./../../../assets/img/kabin1.jpg",
                cabina_balcone_tooltip:"./../../../assets/img/kabin2.jpg",
                suite_tooltip:"./../../../assets/img/kabin3.jpg",

            },

            departure_date:[
                {
                    date:"13 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"14 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"15 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"16 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                {
                    date:"17 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                {
                    date:"18 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                 {
                    date:"19 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                 {
                    date:"20 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                }

            ],

            itinerary:[
                {
                  day_nr:1,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:2,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:3,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:4,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:5,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                }
            ],
            gallery:[
                {   picture:"./../../../assets/img/gall1.jpg",
                    title:"first image"
                },
                {   picture:"./../../../assets/img/gall2.jpg",
                    title:"second image"
                },
                {   picture:"./../../../assets/img/kabin1.jpg",
                    title:"third image"
                },
                {   picture:"./../../../assets/img/kabin2.jpg",
                    title:"fourth image"
                },
            ],          
            review:[
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                },
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                },
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                }
            ] ,
            read_more_link:"#"

    },

];

export let SINGLE_WISHLIST_OFFER =[
    {
            id:1,
            prices:{
                details:{                   
                    destination:"cuba",
                    destination_description:"Cuba, Belize, Honduras, Messico",
                    ship:"MSC    ARMONIA ",
                    port:"LA  HABANA",
                    company_thumb:"./../../../assets/img/kompani/3.jpg",
                    rating_thumb:"./../../../assets/img/rate4.png",
                    destination_thumb: "./../../../assets/img/ofert1.jpg",
                    wishlist_id:"1",
                    checked:"checked"
                },

                cabina_interna_tooltip:"./../../../assets/img/kabin.jpg",
                cabina_esterna_tooltip:"./../../../assets/img/kabin1.jpg",
                cabina_balcone_tooltip:"./../../../assets/img/kabin2.jpg",
                suite_tooltip:"./../../../assets/img/kabin3.jpg",

            },

            departure_date:[
                {
                    date:"13 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"14 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"15 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"16 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                {
                    date:"17 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                {
                    date:"18 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                 {
                    date:"19 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                 {
                    date:"20 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                }

            ],

            itinerary:[
                {
                  day_nr:1,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:2,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"grey",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:3,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:4,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"grey",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:5,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                }
            ],
            gallery:[
                {   picture:"./../../../assets/img/gall1.jpg",
                    title:"first image"
                },
                {   picture:"./../../../assets/img/gall2.jpg",
                    title:"second image"
                },
                {   picture:"./../../../assets/img/kabin1.jpg",
                    title:"third image"
                },
                {   picture:"./../../../assets/img/kabin2.jpg",
                    title:"fourth image"
                },
            ],          
            review:[
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                },
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                },
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                }
            ] ,
            read_more_link:"#"
    },
    {
            id:2,
            prices:{
                details:{                   
                    destination:"Meraviglioso Mediterraneo",
                    destination_description:"Spagna, Francia, Malta ",
                    ship:"Msc Meraviglia",
                    port:"MESSINA",
                    company_thumb:"./../../../assets/img/kompani/1.jpg",
                    rating_thumb:"./../../../assets/img/rate4.png",
                    destination_thumb: "./../../../assets/img/o4.png",
                    wishlist_id:"2",
                    checked:"checked"
                },

                cabina_interna_tooltip:"./../../../assets/img/kabin.jpg",
                cabina_esterna_tooltip:"./../../../assets/img/kabin1.jpg",
                cabina_balcone_tooltip:"./../../../assets/img/kabin2.jpg",
                suite_tooltip:"./../../../assets/img/kabin3.jpg",

            },

            departure_date:[
                {
                    date:"13 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"14 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"15 - 11  - 16",
                    cabina_interna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"600",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"600",
                        link:"#"
                    },
                    suite:{
                        price:"600",
                        link:"#"
                    }          
                },
                {
                    date:"16 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                {
                    date:"17 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                {
                    date:"18 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                 {
                    date:"19 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                },
                 {
                    date:"20 - 11  - 16",
                    cabina_interna:{
                        price:"630",
                        link:"#"
                    },
                    cabina_esterna:{
                        price:"300",
                        link:"#"
                    },
                    cabina_balcone:{
                        price:"630",
                        link:"#"
                    },
                    suite:{
                        price:"603",
                        link:"#"
                    }          
                }

            ],

            itinerary:[
                {
                  day_nr:1,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:2,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:3,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:4,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:5,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                }
            ],
            gallery:[
                {   picture:"./../../../assets/img/gall1.jpg",
                    title:"first image"
                },
                {   picture:"./../../../assets/img/gall2.jpg",
                    title:"second image"
                },
                {   picture:"./../../../assets/img/kabin1.jpg",
                    title:"third image"
                },
                {   picture:"./../../../assets/img/kabin2.jpg",
                    title:"fourth image"
                },
            ],          
            review:[
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                },
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                },
                {
                    thumb:"./../../../assets/img/rate4.png",
                    title:"Lorem ipsum title",
                    description:"Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod . Lorem ipsum dolor sit amet, consecte tur adipiscing elit,sed do eiusmod.",
                    date:"10.11.2016"
                }
            ] ,
            read_more_link:"#"

    },

];

export let  PREVENTIV_DETAILS = {
    id: 1,
    preventiv_nr: "13904",
    company: "Costa Crociere",
    ship: "Costa Diadema",
    formula: "Pensione Completa B.E.",
    departure_date: "26-Jan-2017",
    data_di_rientro:"28-Jan-2017",
    duration: "7",
    port: "CIVITAVECCHIA",
    passengers: "3 + 4",
    total_price:"654",
    prenotation_nr:"123456789",
    itinerary:[
                {
                  day:"1° GIOVEDÌ",
                  port:"CIVITAVECCHIA",
                  arrival:" ",
                  departure:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day:"2° VENERDÌ",
                  port:"CIVITAVECCHIA",
                  arrival:"18.00",
                  departure:"18.00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day:"3° SABATO",
                  port:"SAVONA",
                  arrival:"07:00:00",
                  departure:"17:00:00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day:"4° DOMENICA",
                  port:"MARSIGLIA",
                  arrival:"07:00:00",
                  departure:"17:00:00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day:"5° LUNEDÌ",
                  port:"BARCELLONA",
                  arrival:"07:00:00",
                  departure:"17:00:00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day:"6° MARTEDÌ",
                  port:"PALMA DE MALLORCA",
                  arrival:"07:00:00",
                  departure:"17:00:00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day:"7° MERCOLEDÌ",
                  port:"PALMA DE MALLORCA",
                  arrival:"07:00:00",
                  departure:"17:00:00",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day:"8° GIOVEDÌ",
                  port:"CIVITAVECCHIA",
                  arrival:"07:00:00",
                  departure:"17:00:00",
                  longtitude:45.45,
                  langtitude:466.3
                },
  
            ],
    cabine_image:"./../../../assets/img/room.jpg",
    price_per_adult_cabina_quantity:"2",
    price_per_adult_cabina_price:"479",
    price_per_adult_cabina_total_price:"958",
    price_per_adult_bed_quantity:"1",
    price_per_adult_bed_price:"240",
    price_per_adult_bed_total_price:"240",
    
    price_per_child_quantity:"2",
    price_per_child_price:"479",
    price_per_child_total_price:"958",

    port_taxes_quantity:"7",
    port_taxes_price:"140",
    port_taxes_total_price:"980",

    insurance_taxes_quantity:"7",
    insurance_taxes_price:"140",
    insurance_taxes_total_price:"980",

    entertainment_quantity:"7",
    entertainment_price:"140",
    entertainment_total_price:"980",

    tourist_taxes_quantity:"7",
    tourist_taxes_price:"140",
    tourist_taxes_total_price:"980",

    free_quantity:"7",
    free_price:"140",
    free_total_price:"980",

    total_cruise_price:"2.367",
    confirm_price:"710",
    balance_price:"1.657",
    user : {
        civile_state : "SIG",
        surname : "Ferrari",
        name : "Stefano",
        email : "stefano.ferrari@gmail.com",
        telephone: "456546",
        address : "00144 ROMA RM",
        city : "Roma",
        nationality : "Italy",
        nr: "00144",
        postal_code:"123",
        passangers : [
            {
                civile_state : "Sig",
                surname : "Ferrari",
                name : "Stefano",
                email : "stefano.ferrari@gmail.com",
                telephone: "456546",
                address : "00144 ROMA RM",
                city : "Roma",
                nationality : "Italy",
                nr: "00144",
                postal_code:"123",
                D: "1",
                M: "2",
                Y: "2010",
                doc_type:"Passport",
                doc_number:"123456789",
                expires:"2020"

            },
            {
                civile_state : "Sig",
                surname : "Ferrari",
                name : "Stefano",
                email : "stefano.ferrari@gmail.com",
                telephone: "456546",
                address : "00144 ROMA RM",
                city : "Roma",
                nationality : "Italy",
                nr: "00144",
                postal_code:"123",
                D: "1",
                M: "2",
                Y: "2010",
                doc_type:"Passport",
                doc_number:"123456789",
                expires:"2020"
            },
        ],
        babies : [
            {
                civile_state : "Sig",
                surname : "Ferrari",
                name : "Stefano",
                email : "stefano.ferrari@gmail.com",
                telephone: "456546",
                address : "00144 ROMA RM",
                city : "Roma",
                nationality : "Italy",
                nr: "00144",
                postal_code:"123",
                D: "1",
                M: "2",
                Y: "2010",
                doc_type:"Passport",
                doc_number:"123456789",
                expires:"2020"
            },
            {
                civile_state : "Sig",
                surname : "Ferrari",
                name : "Stefano",
                email : "stefano.ferrari@gmail.com",
                telephone: "456546",
                address : "00144 ROMA RM",
                city : "Roma",
                nationality : "Italy",
                nr: "00144",
                postal_code:"123",
                D: "1",
                M: "2",
                Y: "2010",
                doc_type:"Passport",
                doc_number:"123456789",
                expires:"2020"
            },
        ],
        address_chosed:{
            round: "Secondo Turno",
            surname: "Ferrari",
            name:"Stefano",
            telephone:"+3122332233"
        }


    }

};

export let OFFER_PREVENTIV ={
    id:1,
    destination:"cuba",
    destination_description:"Cuba, Belize, Honduras, Messico",
    ship:"MSC    ARMONIA ",
    port:"LA  HABANA",
    departure_date: "26-Jan-2017",
    duration: "7",

    intern_cabin_name:"CABINA INTERNA",
    intern_cabin_description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pulvinar est neque, eget pulvinar leo sagittis sed, Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    intern_cabin_description_more:"Integer mauris turpis, eleifend a mattis. Bibite analcoliche, cocktail, birre, vini e liquori inclusi in tutti i locali dell'MSC Yacht Club e nei minibar di ogni suite, Area riservata The One Sun Deck, dotata di piscina, vasche idromassaggio, solarium e bar",
    intern_cabin_price:"435",
    intern_checked:"",
    intern_cabin_image:"../../assets/img/kabin1.jpg",

    extern_cabin_name:"CABINA ESTERNA",
    extern_cabin_description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pulvinar est neque, eget pulvinar leo sagittis sed.",
    extern_cabin_description_more:"Integer mauris turpis, eleifend a mattis. Bibite analcoliche, cocktail, birre, vini e liquori inclusi in tutti i locali dell'MSC Yacht Club e nei minibar di ogni suite, Area riservata The One Sun Deck, dotata di piscina, vasche idromassaggio, solarium e bar",
    extern_cabin_price:"335",
    extern_checked:"checked",
    extern_cabin_image:"../../assets/img/kabin.jpg",

    balcony_cabin_name:"CABINA ESTERNA",
    balcony_cabin_description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pulvinar est neque, eget pulvinar leo sagittis sed.",
    balcony_cabin_description_more:"Integer mauris turpis, eleifend a mattis. Bibite analcoliche, cocktail, birre, vini e liquori inclusi in tutti i locali dell'MSC Yacht Club e nei minibar di ogni suite, Area riservata The One Sun Deck, dotata di piscina, vasche idromassaggio, solarium e bar",
    balcony_cabin_price:"335",
    balcony_checked:"",
    balcony_cabin_image:"../../assets/img/kabin2.jpg",

    suite_name:"SUITE",
    suite_description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pulvinar est neque, eget pulvinar leo sagittis sed.",
    suite_description_more:"Integer mauris turpis, eleifend a mattis. Bibite analcoliche, cocktail, birre, vini e liquori inclusi in tutti i locali dell'MSC Yacht Club e nei minibar di ogni suite, Area riservata The One Sun Deck, dotata di piscina, vasche idromassaggio, solarium e bar",
    suite_price:"335",
    suite_checked:"",
    suite_image:"../../assets/img/kabin3.jpg",

    A_longtitude:"51.7519",
    B_longtitude:"-1.2578",
    A_latitude:"50.8429",
    B_latitude:"-0.1313",

    itinerary:[
                {
                  day_nr:1,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"grey",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:2,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:3,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"grey",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:4,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:5,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"grey",
                  longtitude:45.45,
                  langtitude:466.3
                },
                {
                  day_nr:6,
                  date_nr:"13-11-2016",
                  port:"lorem ipsum",
                  arrival:"18.00",
                  parture:"18.00",
                  class:"",
                  longtitude:45.45,
                  langtitude:466.3
                }
            ],
            gallery:[
                {   picture:"./../../../assets/img/gall1.jpg",
                    title:"first image"
                },
                {   picture:"./../../../assets/img/gall2.jpg",
                    title:"second image"
                },
                {   picture:"./../../../assets/img/kabin1.jpg",
                    title:"third image"
                },
                {   picture:"./../../../assets/img/kabin2.jpg",
                    title:"fourth image"
                }
            ],
            related_offer:[
                {   
                    destination:"cuba",
                    destination_description:"Cuba, Belize, Honduras, Messico",
                    ship:"MSC    ARMONIA ",
                    port:"LA  HABANA",
                    company_thumb:"./../../../assets/img/kompani/6.jpg",
                    rating_thumb:"./../../../assets/img/rate4.png",
                    destination_thumb: "./../../../assets/img/ofert1.jpg",
                    wishlist_id:11,
                    checked:""

                },
                {   
                    destination:"dubai",
                    destination_description:"Cuba, Belize, Honduras, Messico",
                    ship:"MSC    ARMONIA ",
                    port:"LA  HABANA",
                    company_thumb:"./../../../assets/img/kompani/9.png",
                    rating_thumb:"./../../../assets/img/rate4.png",
                    destination_thumb: "./../../../assets/img/ofert1.jpg",
                    wishlist_id:12,
                    checked:""
                },
                {   
                    destination:"caraibe",
                    destination_description:"Cuba, Belize, Honduras, Messico",
                    ship:"MSC    ARMONIA ",
                    port:"LA  HABANA",
                    company_thumb:"./../../../assets/img/kompani/2.jpg",
                    rating_thumb:"./../../../assets/img/rate4.png",
                    destination_thumb: "./../../../assets/img/ofert1.jpg",
                    wishlist_id:13,
                    checked:""
                }
            ]

};

export let SINGLE_COMPANY ={
        id:1,
        scetch_img:"./../../../assets/img/ship.jpg",
        title:"LOREM IPSUM TITLE",
        description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod",
        description_more:"Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod",
        read_review_link:"#",
        ships:[
            {
                id:1,
                trips:'Cuba, Africa,Chile , Costa rica, Carrabean',
                image:"./../../../assets/img/kompani/bb1.png",
                rating1:"fa-bookmark",
                rating2:"fa-bookmark",
                rating3:"fa-bookmark",
                rating4:"fa-bookmark-o",
                rating5:"fa-bookmark-o",
                name:"LOREM IPSUM NAME",
                comment_no:"5"
            },
            {
                id:2,
                trips:'Cuba, Africa,Chile,Alaska\'s Inside Passage , Russia',
                image:"./../../../assets/img/kompani/bb2.png",
                rating1:"fa-bookmark",
                rating2:"fa-bookmark",
                rating3:"fa-bookmark-o",
                rating4:"fa-bookmark-o",
                rating5:"fa-bookmark-o",
                name:"LOREM IPSUM mjau",
                comment_no:"0"
            },
            {
                id:3,
                trips:'Russia, Galapagos,Antarctica',
                image:"./../../../assets/img/kompani/bb3.png",
                rating1:"fa-bookmark",
                rating2:"fa-bookmark-o",
                rating3:"fa-bookmark-o",
                rating4:"fa-bookmark-o",
                rating5:"fa-bookmark-o",
                name:"LOREM IPSUM NAME",
                comment_no:"12"
            },
            {
                id:4,
                trips:'Amazon, Africa,Galapagos',
                image:"./../../../assets/img/kompani/bb4.png",
                rating1:"fa-bookmark",
                rating2:"fa-bookmark",
                rating3:"fa-bookmark",
                rating4:"fa-bookmark",
                rating5:"fa-bookmark-o",
                name:"LOREM IPSUM ciu",
                comment_no:"0"
            },
            {
                id:5,
                trips:'Nile River, Africa,Chile',
                image:"./../../../assets/img/kompani/bb5.png",
                rating1:"fa-bookmark",
                rating2:"fa-bookmark",
                rating3:"fa-bookmark",
                rating4:"fa-bookmark-o",
                rating5:"fa-bookmark-o",
                name:"LOREM IPSUM NAME",
                comment_no:"5"
            },
            {
                id:6,
                trips:'Norway, Nile River,Amazon',
                image:"./../../../assets/img/kompani/bb1.png",
                rating1:"fa-bookmark",
                rating2:"fa-bookmark",
                rating3:"fa-bookmark",
                rating4:"fa-bookmark",
                rating5:"fa-bookmark",
                name:"LOREM IPSUM NAME",
                comment_no:"12"
            },
            {
                id:7,
                trips:'Norway, Russia,Chile',
                image:"./../../../assets/img/kompani/bb2.png",
                rating1:"fa-bookmark-o",
                rating2:"fa-bookmark-o",
                rating3:"fa-bookmark-o",
                rating4:"fa-bookmark-o",
                rating5:"fa-bookmark-o",
                name:"LOREM IPSUM NAME",
                comment_no:"4"
            },
            {
                id:8,
                trips:'Galapagos, Chile,Alaska\'s Inside Passage,Norway',
                image:"./../../../assets/img/kompani/bb3.png",
                rating1:"fa-bookmark",
                rating2:"fa-bookmark",
                rating3:"fa-bookmark",
                rating4:"fa-bookmark-o",
                rating5:"fa-bookmark-o",
                name:"LOREM IPSUM NAME",
                comment_no:"0"
            },
            {
                id:9,
                trips:'Cuba, Africa,Chile,Alaska\'s Inside Passage',
                image:"./../../../assets/img/kompani/bb5.png",
                rating1:"fa-bookmark",
                rating2:"fa-bookmark",
                rating3:"fa-bookmark",
                rating4:"fa-bookmark-o",
                rating5:"fa-bookmark-o",
                name:"LOREM IPSUM NAME",
                comment_no:"5"
            }
        ],
        reviews:[
            {
                id:1,
                writer:"John Doe",
                date:"23 - 10 - 2016",
                hour:"19:40 PM",
                rate_img:"./../../../assets/img/rate4.png",
                comment:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus laoreet nisi ut erat consectetur tincidunt. Etiam orci ipsum, eleifend sed urna vitae, sollicitudin porttitor risus. Mauris pharetra arcu accumsan elit viverra dignissim. Duis efficitur, lorem in dictum faucibus, lectus ligula bibendum metus, eu dictum mi ante et nunc. Nullam hendrerit lacus non risus egestas, et pretium sem sagittis. Etiam purus mi, ullamcorper in cursus et, volutpat eu ipsum. Duis quis pellentesque tellus. Quisque eleifend magna eget neque sodales mollis nec ac ipsum. Interdum et malesuada."
            },
            {
                id:2,
                writer:"Ana Marie",
                date:"23 - 10 - 2016",
                hour:"19:40 PM",
                rate_img:"./../../../assets/img/rate4.png",
                comment:"Consectetur adipiscing elit. Etiam orci ipsum, eleifend sed urna vitae, sollicitudin porttitor risus. Mauris pharetra arcu accumsan elit viverra dignissim. Duis efficitur, lorem in dictum faucibus, lectus ligula bibendum metus, eu dictum mi ante et nunc. Nullam hendrerit lacus non risus egestas, et pretium sem sagittis. Etiam purus mi, ullamcorper in cursus et, volutpat eu ipsum. Duis quis pellentesque tellus. Quisque eleifend magna eget neque sodales mollis nec ac ipsum. Interdum et malesuada."
            },
            {
                id:3,
                writer:"Eric Cartman",
                date:"23 - 10 - 2016",
                hour:"19:40 PM",
                rate_img:"./../../../assets/img/rate4.png",
                comment:"Etiam orci ipsum, eleifend sed urna vitae, sollicitudin porttitor risus. Mauris pharetra arcu accumsan elit viverra dignissim. Duis efficitur, lorem in dictum faucibus, lectus ligula bibendum metus, eu dictum mi ante et nunc. Nullam hendrerit lacus non risus egestas, et pretium sem sagittis. Etiam purus mi, ullamcorper in cursus et, volutpat eu ipsum. Duis quis pellentesque tellus. Quisque eleifend magna eget neque sodales mollis nec ac ipsum. Interdum et malesuada."
            }

        ],
        write_review:"#"

};

export let HEADER_DESTINATIONS=[
    {
        id:1,
        title:"Caraibe",
        description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio esse placeat minus fugit, voluptate, quos, ea, nisi.",
        link:"#"
    },
    {
        id:2,
        title:"Mediterranean",
        description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio esse placeat minus fugit, voluptate, quos, ea, nisi.",
        link:""
    },
    {
        id:3,
        title:"Cuba",
        description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio esse placeat minus fugit, voluptate, quos, ea, nisi.",
        link:""
    },
    {
        id:4,
        title:"Bahamas",
        description:"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae distinctio esse placeat minus fugit, voluptate, quos, ea, nisi.",
        link:""
    }

];

export let BLOCK_OFFERS = [
    {
        id:1,
        destination:"Mediterranean",
        title:"Asia e Emirati Arabi Uniti",
        description:"Cuba, Belize, Honduras, Messico",
        company_img:"../assets/img/kompani/1.jpg",
        price:"320",
        link:"#",
        background:"../assets/img/header1.jpg",
        wishlist_id:"1",
        checked:""
    },
    {
        id:2,
        destination:"caraibe",
        title:"AMERICA DEL SUD",
        description:"Nicaragua,Guatemala,Messico,Stati uniti,Perù,Cile ",
        company_img:"../assets/img/kompani/3.jpg",
        price:"520",
        link:"#",
        background:"../assets/img/greece.jpg",
        wishlist_id:"2",
        checked:"checked"
    },
    {
        id:3,
        destination:"destination",
        title:"Attraversando il Canale di Panama",
        description:"Nicaragua, Giamaica, Costa Rica,Panama, Guatemala, Antille Olandesi, Messico, Le isole Cayman, Stati uniti,Cile ",
        company_img:"../assets/img/kompani/5.png",
        price:"780",
        link:"#",
        background:"../assets/img/header3.jpg",
        wishlist_id:"3",
        checked:""
    },
    {
        id:4,
        destination:"Mediterranean",
        title:"Asia e Emirati Arabi Uniti",
        description:"Cuba, Belize, Honduras, Messico",
        company_img:"../assets/img/kompani/1.jpg",
        price:"320",
        link:"#",
        background:"../assets/img/header1.jpg",
        wishlist_id:"4",
        checked:"checked"
    },
    {
        id:5,
        destination:"caraibe",
        title:"AMERICA DEL SUD",
        description:"Nicaragua,Guatemala,Messico,Stati uniti,Perù,Cile ",
        company_img:"../assets/img/kompani/3.jpg",
        price:"520",
        link:"#",
        background:"../assets/img/greece.jpg",
        wishlist_id:"5",
        checked:""
    },
    {
        id:6,
        destination:"destination",
        title:"Attraversando il Canale di Panama",
        description:"Nicaragua, Giamaica, Costa Rica,Panama, Guatemala, Antille Olandesi, Messico, Le isole Cayman, Stati uniti,Cile ",
        company_img:"../assets/img/kompani/5.png",
        price:"780",
        link:"#",
        background:"../assets/img/header3.jpg",
        wishlist_id:"6",
        checked:"checked"
    },
    {
        id:7,
        destination:"Mediterranean",
        title:"Asia e Emirati Arabi Uniti",
        description:"Cuba, Belize, Honduras, Messico",
        company_img:"../assets/img/kompani/1.jpg",
        price:"320",
        link:"#",
        background:"../assets/img/header1.jpg",
        wishlist_id:"7",
        checked:"checked"
    },
    {
        id:8,
        destination:"caraibe",
        title:"AMERICA DEL SUD",
        description:"Nicaragua,Guatemala,Messico,Stati uniti,Perù,Cile ",
        company_img:"../assets/img/kompani/3.jpg",
        price:"520",
        link:"#",
        background:"../assets/img/greece.jpg",
        wishlist_id:"8",
        checked:""
    },
    {
        id:9,
        destination:"destination",
        title:"Attraversando il Canale di Panama",
        description:"Nicaragua, Giamaica, Costa Rica,Panama, Guatemala, Antille Olandesi, Messico, Le isole Cayman, Stati uniti,Cile ",
        company_img:"../assets/img/kompani/5.png",
        price:"780",
        link:"#",
        background:"../assets/img/header3.jpg",
        wishlist_id:"9",
        checked:"checked"
    }
];

export let SINGLE_SHIP = {
    id:1,
    description:"Come tutte le navi costa crociere anche la costa pacifica ha il suo tema che é quello della Musica, ampiamente rappresentato nella decorazione della nave con opere d arte, dai violini antichi alle stampe di partiture fino ad arrivare al nome dei ponti ognuno dei quali ricorda un artista o un opera musicale. La cosa molto interessante che il tema della nave viene ripreso anche nelle sue attività a bordo , è l unica nave che ha una sala di Incisione Musicale con un maestro di musica che si dedica a voi e vi permette di imparare a suonare pianoforte, chitarra e batteria durante il vostro tempo libero.",
    description_more:"Oltre la musica, Costa Pacifica, che come tutte le sue sorelle della Classe Concordia , si rivolge ai viaggiatori che vogliono numerose opzioni di intrattenimento (), una spa di 6000mq e strutture eccellenti per i bambini, tutto viennese-ispirati a un ottimo rapporto qualità-prezzo tariffe.",
    ship_img:"../../assets/img/main-ship.jpg",
    ship_rating:"../assets/img/rate4.png",
    client_rating:"../assets/img/rate0.png",
    cabins:[
                {   picture:"../assets/img/kabin.jpg",
                    title:"first image"
                },
                {   picture:"../assets/img/kabin1.jpg",
                    title:"second image"
                },
                {   picture:"../assets/img/kabin2.jpg",
                    title:"third image"
                },
                {   picture:"../assets/img/kabin3.jpg",
                    title:"fourth image"
                }
    ],
    ponti:[
                {   pont:"../assets/img/ponti/1.png",
                    title:"first image"
                },
                {   pont:"../assets/img/ponti/2.png",
                    title:"second image"
                },
                {   pont:"../assets/img/ponti/3.png",
                    title:"third image"
                }
    ],
    gallery:[
                {   img:"../assets/img/gall1.jpg",
                    title:"first image"
                },
                {   img:"../assets/img/gall2.jpg",
                    title:"second image"
                },
                {   img:"../assets/img/gall1.jpg",
                    title:"third image"
                }
            ], 
    readreview:"#",
    writereview:"#",
    reviews:[
            {
                id:1,
                writer:"John Doe",
                date:"23 - 10 - 2016",
                hour:"19:40 PM",
                rate_img:"./../../../assets/img/rate4.png",
                comment:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus laoreet nisi ut erat consectetur tincidunt. Etiam orci ipsum, eleifend sed urna vitae, sollicitudin porttitor risus. Mauris pharetra arcu accumsan elit viverra dignissim. Duis efficitur, lorem in dictum faucibus, lectus ligula bibendum metus, eu dictum mi ante et nunc. Nullam hendrerit lacus non risus egestas, et pretium sem sagittis. Etiam purus mi, ullamcorper in cursus et, volutpat eu ipsum. Duis quis pellentesque tellus. Quisque eleifend magna eget neque sodales mollis nec ac ipsum. Interdum et malesuada."
            },
            {
                id:2,
                writer:"Ana Marie",
                date:"23 - 10 - 2016",
                hour:"19:40 PM",
                rate_img:"./../../../assets/img/rate4.png",
                comment:"Consectetur adipiscing elit. Etiam orci ipsum, eleifend sed urna vitae, sollicitudin porttitor risus. Mauris pharetra arcu accumsan elit viverra dignissim. Duis efficitur, lorem in dictum faucibus, lectus ligula bibendum metus, eu dictum mi ante et nunc. Nullam hendrerit lacus non risus egestas, et pretium sem sagittis. Etiam purus mi, ullamcorper in cursus et, volutpat eu ipsum. Duis quis pellentesque tellus. Quisque eleifend magna eget neque sodales mollis nec ac ipsum. Interdum et malesuada."
            },
            {
                id:3,
                writer:"Eric Cartman",
                date:"23 - 10 - 2016",
                hour:"19:40 PM",
                rate_img:"./../../../assets/img/rate4.png",
                comment:"Etiam orci ipsum, eleifend sed urna vitae, sollicitudin porttitor risus. Mauris pharetra arcu accumsan elit viverra dignissim. Duis efficitur, lorem in dictum faucibus, lectus ligula bibendum metus, eu dictum mi ante et nunc. Nullam hendrerit lacus non risus egestas, et pretium sem sagittis. Etiam purus mi, ullamcorper in cursus et, volutpat eu ipsum. Duis quis pellentesque tellus. Quisque eleifend magna eget neque sodales mollis nec ac ipsum. Interdum et malesuada."
            }

        ]
};

export let BLOG_LIST = [
    {
        id:1,
        news_title:"Novità per l’estate 2017 con Costa crociere",
        news_image:"../assets/img/header1.jpg",
        news_excerpt:"Due nuovi itinerari nel Mediterraneo, il primo con Costa neoClassica dal 13 maggio con partenza dal porto di Bari verso le più belle isole della Grecia. L’itinerario comprende le isole di Corfù, Creta, Santorini, con una sosta lunga fino a mezzanotte, e infine Mykonos, dove la nave si fermerà un intera notte per godersi la vita notturna e le spiagge dell’isola.",
        news_link:"#"
    },
    {
        id:2,
        news_title:"Norwegian Ships by Size – Biggest to Smallest Ships",
        news_image:"../assets/img/header2.jpg",
        news_excerpt:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget tempor tortor, sit amet convallis dui. Praesent in nibh pulvinar, semper elit eget, lacinia mi. Aenean vulputate fermentum diam, eget laoreet leo mollis a. Quisque sodales efficitur nunc, ut porta mi sodales non. Nullam feugiat ullamcorper urna quis condimentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        news_link:"#"
    },
    {
        id:3,
        news_title:"Introducing… Ship Mate 4.0 (queue parade & flyover)",
        news_image:"../assets/img/o1.png",
        news_excerpt:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget tempor tortor, sit amet convallis dui. Praesent in nibh pulvinar, semper elit eget, lacinia mi. Aenean vulputate fermentum diam, eget laoreet leo mollis a. Quisque sodales efficitur nunc, ut porta mi sodales non. Nullam feugiat ullamcorper urna quis condimentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        news_link:"#"
    },
    {
        id:4,
        news_title:"LOREM IPSUM",
        news_image:"../assets/img/gall2.jpg",
        news_excerpt:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget tempor tortor, sit amet convallis dui. Praesent in nibh pulvinar, semper elit eget, lacinia mi. Aenean vulputate fermentum diam, eget laoreet leo mollis a. Quisque sodales efficitur nunc, ut porta mi sodales non. Nullam feugiat ullamcorper urna quis condimentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        news_link:"#"
    },
    {
        id:5,
        news_title:"LOREM IPSUM",
        news_image:"../assets/img/gall2.jpg",
        news_excerpt:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget tempor tortor, sit amet convallis dui. Praesent in nibh pulvinar, semper elit eget, lacinia mi. Aenean vulputate fermentum diam, eget laoreet leo mollis a. Quisque sodales efficitur nunc, ut porta mi sodales non. Nullam feugiat ullamcorper urna quis condimentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
        news_link:"#"
    }
];

export let SINGLE_BLOG ={
    id:"1",
    title:"LOREM IPSUM",
    p_1:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec arcu luctus, accumsan nibh sit amet, egestas nulla. In volutpat porta nisl, et faucibus elit tempus ac. Nulla eu vehicula purus. Integer faucibus diam in fringilla lobortis. Integer porttitor eu magna nec sagittis. Mauris varius posuere lacinia. Nulla sollicitudin imperdiet elit et luctus. Ut ut blandit erat. Phasellus nisl neque, finibus vel dolor eget, sollicitudin tristique nunc. Vestibulum bibendum dignissim sollicitudin. Mauris libero velit, malesuada sit amet auctor quis, lacinia sit amet ligula.Morbi nisi metus, tempus interdum justo sed, imperdiet tempor sem. In ornare enim ac diam maximus, et sodales nisi egestas. Ut lobortis justo erat, vel volutpat sapien semper nec. Nunc vel diam in est volutpat porttitor. Donec ut pulvinar enim. Nam at tortor egestas, molestie nisl eu, tincidunt est. Proin non elit nisl.Morbi nisi metus, tempus interdum justo sed, imperdiet tempor sem. In ornare enim ac diam maximus, et sodales nisi egestas. Ut lobortis justo erat, vel volutpat sapien semper nec. Nunc vel diam in est volutpat porttitor. Donec ut pulvinar enim. Nam at tortor egestas, molestie nisl eu, tincidunt est. Proin non elit nisl.",
    p_2:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec arcu luctus, accumsan nibh sit amet, egestas nulla. In volutpat porta nisl, et faucibus elit tempus ac. Nulla eu vehicula purus. Integer faucibus diam in fringilla lobortis. Integer porttitor eu magna nec sagittis. Mauris varius posuere lacinia. Nulla sollicitudin imperdiet elit et luctus. Ut ut blandit erat. Phasellus nisl neque, finibus vel dolor eget, sollicitudin tristique nunc. Vestibulum bibendum dignissim sollicitudin. Mauris libero velit, malesuada sit amet auctor quis, lacinia sit amet ligula.Morbi nisi metus, tempus interdum justo sed, imperdiet tempor sem. In ornare enim ac diam maximus, et sodales nisi egestas. Ut lobortis justo erat, vel volutpat sapien semper nec. Nunc vel diam in est volutpat porttitor. Donec ut pulvinar enim. Nam at tortor egestas, molestie nisl eu, tincidunt est. Proin non elit nisl.Morbi nisi metus, tempus interdum justo sed, imperdiet tempor sem. In ornare enim ac diam maximus, et sodales nisi egestas. Ut lobortis justo erat, vel volutpat sapien semper nec. Nunc vel diam in est volutpat porttitor. Donec ut pulvinar enim. Nam at tortor egestas, molestie nisl eu, tincidunt est. Proin non elit nisl.",
    p_3:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec arcu luctus, accumsan nibh sit amet, egestas nulla. In volutpat porta nisl, et faucibus elit tempus ac. Nulla eu vehicula purus. Integer faucibus diam in fringilla lobortis. Integer porttitor eu magna nec sagittis. Mauris varius posuere lacinia. Nulla sollicitudin imperdiet elit et luctus. Ut ut blandit erat. Phasellus nisl neque, finibus vel dolor eget, sollicitudin tristique nunc. Vestibulum bibendum dignissim sollicitudin. Mauris libero velit, malesuada sit amet auctor quis, lacinia sit amet ligula.Morbi nisi metus, tempus interdum justo sed, imperdiet tempor sem. In ornare enim ac diam maximus, et sodales nisi egestas. Ut lobortis justo erat, vel volutpat sapien semper nec. Nunc vel diam in est volutpat porttitor. Donec ut pulvinar enim. Nam at tortor egestas, molestie nisl eu, tincidunt est. Proin non elit nisl.Morbi nisi metus, tempus interdum justo sed, imperdiet tempor sem. In ornare enim ac diam maximus, et sodales nisi egestas. Ut lobortis justo erat, vel volutpat sapien semper nec. Nunc vel diam in est volutpat porttitor. Donec ut pulvinar enim. Nam at tortor egestas, molestie nisl eu, tincidunt est. Proin non elit nisl.",
    image:"../assets/img/gall1.jpg",
    date:"01.12.2017",
    similar:[
        {
            id:1,
            news_title:"Novità per l’estate 2017 con Costa crociere",
            news_image:"../assets/img/header1.jpg",
            news_excerpt:"Due nuovi itinerari nel Mediterraneo, il primo con Costa neoClassica dal 13 maggio con partenza dal porto di Bari verso le più belle isole della Grecia. L’itinerario comprende le isole di Corfù, Creta, Santorini, con una sosta lunga fino a mezzanotte, e infine Mykonos, dove la nave si fermerà un intera notte per godersi la vita notturna e le spiagge dell’isola. Fermerà un intera notte per godersi la vita notturna e le spiagge dell’isola.",
            news_link:"#"
        },
        {
            id:2,
            news_title:"Norwegian Ships by Size – Biggest to Smallest Ships",
            news_image:"../assets/img/header2.jpg",
            news_excerpt:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget tempor tortor, sit amet convallis dui. Praesent in nibh pulvinar, semper elit eget, lacinia mi. Aenean vulputate fermentum diam, eget laoreet leo mollis a. Quisque sodales efficitur nunc, ut porta mi sodales non. Nullam feugiat ullamcorper urna quis condimentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
            news_link:"#"
        },
        {
            id:3,
            news_title:"Introducing… Ship Mate 4.0 (queue parade & flyover)",
            news_image:"../assets/img/o1.png",
            news_excerpt:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget tempor tortor, sit amet convallis dui. Praesent in nibh pulvinar, semper elit eget, lacinia mi. Aenean vulputate fermentum diam, eget laoreet leo mollis a. Quisque sodales efficitur nunc, ut porta mi sodales non. Nullam feugiat ullamcorper urna quis condimentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
            news_link:"#"
        },
        {
            id:4,
            news_title:"LOREM IPSUM",
            news_image:"../assets/img/gall2.jpg",
            news_excerpt:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget tempor tortor, sit amet convallis dui. Praesent in nibh pulvinar, semper elit eget, lacinia mi. Aenean vulputate fermentum diam, eget laoreet leo mollis a. Quisque sodales efficitur nunc, ut porta mi sodales non. Nullam feugiat ullamcorper urna quis condimentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
            news_link:"#"
        },
    ]
};

export let SHIP_REVIEW = {
    id:1,
    ship:"MSC ARMONIA",
    image:"../assets/img/gall1.jpg",
    title:"LOREM IPSUM",
    description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus.",
    description_more:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus.",
    valutazione_c:"1",
    valutazione_u:"2",
    cabina_c:"3",
    cabina_u:"4",
    fitness_c:"1",
    fitness_u:"2",
    cucina_c:"3",
    cucina_u:"1",
    aree_c:"1",
    aree_u:"2",
    imbarco_c:"3",
    imbarco_u:"1",
    servizio_c:"2",
    servizio_u:"3",
    pulizia_c:"1",
    pulizia_u:"2",
    rapporto_c:"0",
    raportto_u:"2",
    divertimento_c:"1",
    divertimento_u:"1",
    single_user:[
        {
            id:1,
            rating_img:"../assets/img/rate4.png",
            rate_nr:"4.3",
            user:"Stan Marsh",
            comment:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            comment_more:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            user_img:"../assets/img/abi.jpg"
        },
        {
            id:2,
            rating_img:"../assets/img/rate5.png",
            rate_nr:"5.0",
            user:"Stan Marsh",
            comment:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            comment_more:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            user_img:"../assets/img/abi.jpg"
        },
        {
            id:3,
            rating_img:"../assets/img/rate0.png",
            rate_nr:"0.0",
            user:"Stan Marsh",
            comment:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            comment_more:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            user_img:"../assets/img/abi.jpg"
        },
        {
            id:4,
            rating_img:"../assets/img/rate2.png",
            rate_nr:"2.3",
            user:"Stan Marsh",
            comment:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            comment_more:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            user_img:"../assets/img/abi.jpg"
        },
        {
            id:5,
            rating_img:"../assets/img/rate3.png",
            rate_nr:"3.1",
            user:"Stan Marsh",
            comment:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            comment_more:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra quam lorem, eget sollicitudin tellus suscipit eu. In hac habitasse platea dictumst. Sed eget lacus at leo sagittis faucibus at a augue. Nullam pellentesque, nibh sit amet aliquet elementum, nibh ligula condimentum erat, a pharetra metus nibh non diam. Sed non pharetra tellus. Quisque pellentesque quis mi sed faucibus.",
            user_img:"../assets/img/abi.jpg"
        },
    ]

};
export let SINGLE_OFFER = {
    title : "Lorem ipsum dolor.",
   description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla maiores voluptatibus, expedita. Dolor quia ex sapiente magnam non ad iste harum, consequatur eos laudantium aperiam quidem, reprehenderit impedit tempora eligendi? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe veritatis ex velit asperiores molestiae ab veniam quia totam, quibusdam amet harum nemo incidunt, delectus ipsam aut maiores nesciunt dolores autem."
};