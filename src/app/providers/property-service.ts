import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, RequestOptions, Jsonp, URLSearchParams } from '@angular/http';
import { SERVER_URL } from './config';
import 'rxjs/Rx';

let homeSliderURL = SERVER_URL + '/homeslider',
    offersURL     = SERVER_URL + '/offers',
    reviewsURL    = SERVER_URL + '/reviews',
    companiesURL  = SERVER_URL + '/companies',
    articlesURL   = SERVER_URL + '/articles',
    searchURL     = SERVER_URL + '/search';

@Injectable()
export class PropertyService {
    apikey: string;
    constructor(public http: Http, private _jsonp: Jsonp) {
        this.http = http;
        this.apikey = 'fed69657ba4cc6e1078d2a6a95f51c8c';
        console.log('Property service is ready');
    }

    // find all offers and stuff
    findAllOffers() {
        return this.http.get(offersURL)
            .map(res => res.json())
            .toPromise();
    }

    findOfferByType(type: string = 'destination') {
        type = type.toLowerCase();
        return this.http.get(offersURL + "?offer_type=" + type)
          .map(res => res.json())
          .toPromise();
    }

    findOfferByById(id: number = 0) {
        return this.http.get(offersURL+"/"+id)
            .map(res => res.json())
            .toPromise();
    }

    findByName(searchKey:string = '') {
      let key: string = searchKey.toLowerCase();
        return this.http.get(offersURL + "?key=" + key)
            .map(res => res.json())
            .toPromise();
    }

    // find home slider relatede data
    getHomeSliderImages() {
      return this.http.get(homeSliderURL)
            .map(res => res.json())
            .toPromise();
    }

    // Find company related offer
    findCompanyList() {
      return this.http.get(companiesURL)
            .map(res => res.json())
            .toPromise();
    }

    findOffersByCompany(id: number = 0) {
      return this.http.get(companiesURL+"/"+id)
          .map(res => res.json())
          .toPromise();
    }

    getAllReviews() {
        return this.http.get(reviewsURL)
            .map(res => res.json())
            .toPromise();
    }

    getLatestReviews() {
        return this.http.get(reviewsURL+"/latest")
            .map(res => res.json())
            .toPromise();
    }

    getReview(id: number) {
        return this.http.get(reviewsURL+"/"+id)
            .map(res => res.json())
            .toPromise();
    }


    findLatestArticle() {
      return this.http.get(articlesURL+"/latest")
          .map(res => res.json())
          .toPromise();
    }

    findAllArticles() {
      return this.http.get(articlesURL)
          .map(res => res.json())
          .toPromise();
    }

    findArticle(id: number) {
      return this.http.get(articlesURL+"/"+id)
          .map(res => res.json())
          .toPromise();
    }

    getSearchResultOffer(params= null){

       return this._jsonp.get(searchURL, params)
          .map(res => res.json())
          .toPromise();
    
    }


    searchOffer(searchStr: string) {
      var search = new URLSearchParams();
      search.set('sort_by','popularity.desc');
      search.set('query', searchStr);
       return this._jsonp.get(searchURL, {search})
          .map(res => res.json())
          .toPromise();
    }
}
