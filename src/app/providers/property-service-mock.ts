import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
// mock data
import { OFFERS, SLIDES, COMPANY, COMPANY_LIST, ARTICLE_LIST, REVIEW, OFFER_CATEGORY, LINK_BAR, SINGLE_SEARCH_OFFER, SINGLE_WISHLIST_OFFER, PREVENTIV_DETAILS, OFFER_PREVENTIV, SINGLE_COMPANY,HEADER_DESTINATIONS, BLOCK_OFFERS, SINGLE_SHIP, BLOG_LIST, SINGLE_BLOG, SHIP_REVIEW ,SINGLE_OFFER} from './mock-data';


@Injectable()
export class PropertyService {
    constructor() {
        console.log('PropertyService service is ready');
    }

	findAllOffers() {
		return Promise.resolve(OFFERS);
	}


	findOfferByType(type: string = 'destination') {
		let key: string = type.toLowerCase();
		return Promise.resolve(OFFERS[key]);
	}

	// home slider data
	getHomeSliderImages() {
		return Promise.resolve(SLIDES);
	    // return Observable.create(observer => {
	    //     observer.next(SLIDES);
	    //     observer.complete();
	    // });
	}

	 // Find company related offer
	findCompanyList() {
		return Promise.resolve(COMPANY_LIST);
	}

	findOffersByCompany(id: any = 0) {
		return Promise.resolve(COMPANY[id]);
	}

	// find review related data
	getReview(id: any = '') {
		return Promise.resolve(REVIEW);
	}

	// Find blog related data
	findLatestArticle() {
		// here we select only 2 articles
		return Promise.resolve(ARTICLE_LIST);
	}

	findAllArticles() {
		return Promise.resolve(ARTICLE_LIST);
	}

	findArticle(id: number) {
		return Promise.resolve(ARTICLE_LIST[id]);
	}


	getOfferCategories(params = null) {
		return Promise.resolve(OFFER_CATEGORY);
	}

	getLinkBar() {
		return Promise.resolve(LINK_BAR);
	}

	getSearchResultOffer(params = null) {

		if(Object.keys(params).length === 0 && params.constructor === Object)
		{
			return Promise.resolve(SINGLE_SEARCH_OFFER);
		}
		else{
			var data ;
			return Promise.resolve(SINGLE_SEARCH_OFFER).then(response => {
				data = response.filter(function(resp){
					 
					 return resp;
				})
				return data;
			});
		}
		
	}
	getSingleOffer(offer)
	{
		return Promise.resolve(SINGLE_OFFER);
	}

	getWishlistResultOffer() {
		return Promise.resolve(SINGLE_WISHLIST_OFFER);
	}
	

	setOfferPreventiv() {
		return Promise.resolve(OFFER_PREVENTIV);
	}

	getPreventiveDetails(params = {}) {
		if(Object.keys(params).length === 0 && params.constructor === Object)
		{
			return Promise.resolve(PREVENTIV_DETAILS);
		}
		else{
			var data ;
			return Promise.resolve(PREVENTIV_DETAILS).then(response => {
					if(response.id == params['id'])
					{
						return response;
					}
			});
		}
	}

	getSingleCompany(params = {}) {
		if(Object.keys(params).length === 0 && params.constructor === Object)
		{
			return Promise.resolve(SINGLE_COMPANY);
		}
		else{
			var data ;
			return Promise.resolve(SINGLE_COMPANY).then(response => {
				var ships = response.ships;
				data = ships.filter((resp)=>{
					if( (!!params['company'] && resp.name.toLowerCase() == params['company'].toLowerCase()) 
						|| ( !!params['place'] && resp.trips.toLowerCase().includes(params['place'].toLowerCase()))
						 || ( !!params['subplace'] && resp.trips.toLowerCase().includes(params['subplace'].toLowerCase())) )
					{
					 	return resp;
					}
				})
				response.ships = data;
				return response;
			});
		}
	}

	getHeaderPinDestinations(){
		return Promise.resolve(HEADER_DESTINATIONS);
	}

	putThreeBlockOffer(params = {}){
		if(Object.keys(params).length === 0 && params.constructor === Object)
		{
			return Promise.resolve(BLOCK_OFFERS);
		}
		else{
			var data ;
			return Promise.resolve(BLOCK_OFFERS).then(response => {
				data = response.filter((resp)=>{
					
					if(resp.destination.toLowerCase() == params['destination'].toLowerCase() 
						|| ( !!params['season'] && resp.description.toLowerCase().includes(params['season'].toLowerCase()))
						 || ( !!params['place'] && resp.description.toLowerCase().includes(params['place'].toLowerCase())) 
						 || ( !!params['subplace'] && resp.description.toLowerCase().includes(params['subplace'].toLowerCase())) 
						 || ( !!params['envoirment'] && resp.description.toLowerCase().includes(params['envoirment'].toLowerCase())))
					{
					 	return resp;
					}
				})
				return data;
			});
		}
	}

	getSingleShip(){
		return Promise.resolve(SINGLE_SHIP);
	}

	getBlogList(){
		return Promise.resolve(BLOG_LIST);
	}

	getSingleBlog(){
		return Promise.resolve(SINGLE_BLOG);
	}

	getShipReview(){
		return Promise.resolve(SHIP_REVIEW);
	}

}
