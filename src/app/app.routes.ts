import { Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { OfferteComponent } from './pages/offerte/offerte.component';
import { SearchResultComponent } from './pages/search-result/search-result.component';
import { PreventiveDetailsComponent } from './pages/preventive-details/preventive-details.component';
import { MakePreventivComponent } from './pages/make-preventiv/make-preventiv.component';
import { WishlistComponent } from './pages/wishlist/wishlist.component';
import { CompaniesListComponent } from './pages/companies-list/companies-list.component';
import { SingleCompanyComponent } from './pages/single-company/single-company.component';
import { DestinationListComponent } from './pages/destination-list/destination-list.component';
import { SingleDestinationComponent } from './pages/single-destination/single-destination.component';
import { SingleShipComponent } from './pages/single-ship/single-ship.component';
import { LastMinOfferComponent } from './pages/last-min-offer/last-min-offer.component';
import { BlogComponent } from './pages/blog/blog.component';
import { SingleBlogComponent } from './pages/single-blog/single-blog.component';
import { ShipReviewComponent } from './pages/ship-review/ship-review.component';
import { AddReviewComponent } from './pages/add-review/add-review.component';
import { PageFooterComponent } from './pages/page-footer/page-footer.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';
import { PreventiveAfterComponent } from './pages/preventive-after/preventive-after.component';
import { SingleOfferComponent } from './pages/single-offer/single-offer.component';


export const appRoutes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'offerte-crociere', component: OfferteComponent},
    {path: 'search-result', component: SearchResultComponent},
    {path: 'wishlist', component: WishlistComponent},
    {path: 'companies-list', component: CompaniesListComponent},
    {path: 'destination-list', component: DestinationListComponent},
    {path: 'single-company', component: SingleCompanyComponent},
    {path: 'single-destination', component: SingleDestinationComponent },
    {path: 'single-ship', component: SingleShipComponent },
    {path: 'blog', component: BlogComponent },
    {path: 'subscribe', component: SubscribeComponent },
    {path: 'page-footer', component: PageFooterComponent },
    {path: 'add-review', component: AddReviewComponent },
    {path: 'single-blog', component: SingleBlogComponent },
    {path: 'ship-review', component: ShipReviewComponent},
    {path: 'last-min-offer', component: LastMinOfferComponent},
    {path: 'preventive-details', component: PreventiveDetailsComponent},
    {path: 'preventive-after', component: PreventiveAfterComponent},
    {path: 'make-preventiv', component: MakePreventivComponent},
    {path: 'offerte-crociere-lastminute', component: LastMinOfferComponent},
    {path: 'offerte-crociere/destinazione', component: DestinationListComponent},
    {path: 'offerte-crociere/destinazione/:destination', component: SingleDestinationComponent},
    {path: 'offerte-crociere/compagnia', component: CompaniesListComponent},
    {path: 'offerte-crociere/compagnia/:company', component: SingleCompanyComponent},
    {path: 'offerte-crociere/speciale', component: OfferteComponent},
    {path: 'offerte-crociere/speciale/:event', component: OfferteComponent},
    {path: 'crociere', component: DestinationListComponent},
    {path: 'crociera', component: DestinationListComponent},
    {path: 'crociera/:destination/:season/:place/:subplace/:envoirment', component: SingleDestinationComponent},
    {path: 'preventive/:id/:hash', component: PreventiveDetailsComponent},
    {path: 'compagnie', component: CompaniesListComponent},
    {path: 'compagnia/:place', component: SingleCompanyComponent},
    {path: 'compagnia/:place/:subplace', component: SingleCompanyComponent},
    {path: 'destinazioni', component: DestinationListComponent},
    {path: 'crociere-destinazioni/:destination', component: SingleDestinationComponent},

    //New page Routes.
    {path: 'destination-offer/:offer', component: SingleOfferComponent},
    {path: 'company-offer/:offer', component: SingleOfferComponent},
    {path: 'special-event-offer/:offer', component: SingleOfferComponent},
    //End New page routes.
    
    {path: '', component: HomeComponent, pathMatch: 'full'} // redirect to home page on load
];