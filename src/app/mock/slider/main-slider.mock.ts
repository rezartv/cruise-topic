import { MainSlider } from './../../models/slider';
export var SLIDES: MainSlider[] = [
	{ caption: 'Mr. Nice', src: "" },
	{ caption: 'Narco', src: "" },
	{ caption: 'Bombasto', src: "" },
	{ caption: 'Celeritas', src: "" },
	{ caption: 'Magneta', src: "" },
	{ caption: 'RubberMan', src: "" },
	{ caption: 'Dynama', src: "" },
	{ caption: 'Dr IQ', src: "" },
	{ caption: 'Magma', src: "" },
	{ caption: 'Tornado', src: "" }
];