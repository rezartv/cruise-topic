export let PROPERTIES = [
	{
		id: 1,
		type: "destination",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
		price: "$475,000",
		title: "Destination",
		pic: "./../../../assets/img/oferte.jpg",
		thumb: "./../../../assets/img/oferte.jpg",
		offers: [
			{
				id: 1,
				title: "ANTILLE DA SCOPRIRE",
				description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
				duration: "8 giorni 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "320€",
				link: "#",
				company_name: "Celebrity crules",
				company_logo: "./../../../assets/img/kompani/1.jpg" 
			},
			{
				id: 1,
				title: "Cuba Jamaica e Honduras",
				description: "Cuba, Giamaica, Isole Cayman, Messico.",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "340€",
				link: "#",
				company_name: "Costa",
				company_logo: "./../../../assets/img/kompani/2.jpg"        		
			},
			{
				id: 1,
				title: "L'Havana e le altre perle dei Caraibi",
				description: "Cuba, Belize, Honduras e Messico.",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "360€",
				link: "#",
				company_name: "MSC Croiciere",
				company_logo: "./../../../assets/img/kompani/3.jpg"        		
			},
			{
				id: 1,
				title: "Sole d'inverno",
				description: "Spagna Francia e Baleari",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: '380€',
				link: "#",
				company_name: "Princess Cruises",
				company_logo: "./../../../assets/img/kompani/4.jpg"        		
			}
		]
	},
	{
		id: 2,
		type: "company",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
		price: "$475,000",
		title: "company",
		pic: "https://s3-us-west-1.amazonaws.com/sfdc-demo/realty/house08wide.jpg",
		thumb: "https://s3-us-west-1.amazonaws.com/sfdc-demo/realty/house08sq.jpg",
		offers: [
			{
				id: 1,
				title: "Sole d'inverno",
				description: "Spagna Francia e Baleari",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: '$1,200,000',
				company_name: "Princess Cruises",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 2,
				title: "Cuba Jamaica e Honduras",
				description: "Cuba, Giamaica, Isole Cayman, Messico.",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "$1,200,000",
				company_name: "Costa",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 3,
				title: "ANTILLE DA SCOPRIRE",
				description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
				duration: "8 giorni 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "320€",
				company_name: "Celebrity crules",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 4,
				title: "L'Havana e le altre perle dei Caraibi",
				description: "Cuba, Belize, Honduras e Messico.",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "$1,200,000",
				company_name: "MSC Croiciere",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			}
			
		]
	},
		{
		id: 3,
		type: "destination",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
		price: "$475,000",
		title: "Destination",
		pic: "./../../../assets/img/ofert1.jpg",
		thumb: "./../../../assets/img/ofert1.jpg",
		offers: [
			{
				id: 1,
				title: "L'Havana e le altre perle dei Caraibi",
				description: "Cuba, Belize, Honduras e Messico.",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "$1,200",
				company_name: "MSC Croiciere",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 2,
				title: "Sole d'inverno",
				description: "Spagna Francia e Baleari",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: '$1,200,000',
				company_name: "Princess Cruises",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 3,
				title: "ANTILLE DA SCOPRIRE",
				description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
				duration: "8 giorni 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "420€",
				company_name: "Celebrity crules",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 4,
				title: "Cuba Jamaica e Honduras",
				description: "Cuba, Giamaica, Isole Cayman, Messico.",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "$500",
				company_name: "Costa",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			}
		]
	},
	{
		id: 4,
		type: "port",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad.",
		price: "$475,000",
		title: "Porto",
		pic: "./../../../assets/img/o4.png",
		thumb: "./../../../assets/img/o4.png",
		offers: [
			{
				id: 1,
				title: "Cuba Jamaica e Honduras",
				description: "Cuba, Giamaica, Isole Cayman, Messico.",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "$1,200,000",
				company_name: "Costa",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 2,
				title: "L'Havana e le altre perle dei Caraibi",
				description: "Cuba, Belize, Honduras e Messico.",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "$1,200,000",
				company_name: "MSC Croiciere",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 3,
				title: "Sole d'inverno",
				description: "Spagna Francia e Baleari",
				duration: "8 Giorni, 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: '$1,200,000',
				company_name: "Princess Cruises",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			},
			{
				id: 4,
				title: "ANTILLE DA SCOPRIRE",
				description: "Martinica, Guadalupe, Saint Lucia, Barbados, Trinidad e Tobago, Grenada, Dominica.",
				duration: "8 giorni 7 notti",
				departure: "Genova",
				dates: "16-17-18",
				price: "320€",
				company_name: "Celebrity crules",
				company_logo: "https://s3-us-west-1.amazonaws.com/sfdc-demo/people/michael_jones.jpg"        		
			}
		]
	}
];
