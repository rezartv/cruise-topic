import { MainSlider } from './../models/slider';
export var SLIDES: MainSlider[] = [
	{ caption: 'Mr. Nice', src: "./../../../assets/img/header1.jpg" },
	{ caption: 'Narco', src: "./../../../assets/img/header2.jpg" },
	{ caption: 'Bombasto', src: "./../../../assets/img/header3.jpg" },
	{ caption: 'Celeritas', src: "./../../../assets/img/header4.jpg" }
];