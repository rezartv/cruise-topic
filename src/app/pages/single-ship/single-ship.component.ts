import { Component, OnInit } from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';

@Component({
  selector: 'app-single-ship',
  templateUrl: './single-ship.component.html',
  styleUrls: ['./single-ship.component.css']
})
export class SingleShipComponent implements OnInit {

  singleShip: any;
  loader:boolean = false;
  options = {
              loop:true,
              nav:true,
            responsive:{
                      0:{
                          items:1
                      },
                      600:{
                          items:2
                      },
                      1000:{
                          items:3
                      }
                  }
          };
	constructor(private _propertyService: PropertyService) { 
    this.loader = true;
		this._propertyService.getSingleShip().then(response => {
			this.singleShip = response;
      this.loader = false;
		});
	}

  ngOnInit() {
  }

}
