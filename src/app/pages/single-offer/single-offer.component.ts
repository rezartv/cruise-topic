import {  ViewChild, ElementRef, AfterViewInit, Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
// models
import { Search } from './../../models/search'; // model
// pipes
import { PrettyPrintPipe } from './../../pipes/prettyprint';
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';
import { ActivatedRoute } from '@angular/router';
declare var jQuery: any;

@Component({
	selector: 'app-single-offer',
	templateUrl: './single-offer.component.html',
	styleUrls: ['./single-offer.component.css'],
})
export class SingleOfferComponent implements AfterViewInit { 
	// slider
	sliders: any;
	// search form
	search = new Search('ALL', 'ALL', 'ALL', 'ALL');

	singleOffers: any;
	
	// offerte
	currentOffer: any;
	offerTypes: Array<string> = ['destination', 'company', 'sezonal', 'port']; 
	nextOfferType : string = 'company';
	// reviews
	review: any;
	// offers related to company
	companyOffer: any;
	companyList: any;
	// blog data
	news: any;
	// extra variables
	isInitialised = false;
	loader:boolean = false;
	shipLoader = false;
	dataLoader = false;
	loginOpen = false;
	registerOpen=false;
	singleShip: any;
	singleOffer:any;
    shipOptions = {
              loop:true,
              nav:true,
              items:1
          };
	options = {
			items:1,
			loop:true,
			navText: [
			"<i class='fa fa-angle-right' aria-hidden='true'></i>",
			"<i class='fa fa-angle-left' aria-hidden='true'></i>"
			]
		};
	options2={ margin: 5, loop:true , autoWidth:true ,autoplay: true,navText: ["<i class='fa fa-angle-right' aria-hidden='true'></i>","<i class='fa fa-angle-left' aria-hidden='true'></i>"] };
	

	@ViewChild('companies') companies: ElementRef;
	@ViewChild('offers') offers: ElementRef;
	@ViewChild('testimonial') testimonial: ElementRef;
	constructor(private _propertyService: PropertyService, public elRef: ElementRef,private route : ActivatedRoute ) { 

		this.loader = true;
		var i= 0;
		this._propertyService.getHomeSliderImages().then(response => {
			this.sliders = response;
			i++;
			if(i == 4)
			{
				this.loader = false;
			}
		});

		this._propertyService.findOfferByType('destination').then(response => {
			this.currentOffer = response;
			i++;
			if(i == 4)
			{
				this.loader = false;
			}
		});

		this._propertyService.findCompanyList().then(response => {
			this.companyList = response;
			i++;
			if(i == 4)
			{
				this.loader = false;
			}
		});

		this._propertyService.findOffersByCompany(0).then(response => {
			this.companyOffer = response;
			i++;
			if(i == 4)
			{
				this.loader = false;
			}
		});
	}


	ngOnInit() {
		this.loader = true;
		this.shipLoader = true;
		var i= 0;
		this._propertyService.findLatestArticle().then(response => {
			this.news = response;
			i++;
			if(i == 2)
			{
				this.loader = false;
			}
		});

		this._propertyService.getReview(2).then(response => {
			this.review = response;
			i++;
			if(i == 2)
			{
				this.loader = false;
			}
		});
		this._propertyService.getSingleShip().then(response => {
			this.singleShip = response;
	      this.shipLoader = false;
			});

		this.dataLoader = true;
		this.route.params.subscribe(params => {
		//Get the offer
		this._propertyService.getSingleOffer(params['offer']).then(response => {
			this.singleOffer = response;
			console.log(this.singleOffer);
			setTimeout(()=>{
				this.dataLoader = false;
			},3000);
		});
	});	
	}


	ngAfterViewInit() {
		let that = this;
		let options = {
			loop:true,
			margin:10,
			nav:true,
			autoPlay : true,
			navText: [
			"<i class='fa fa-angle-right' aria-hidden='true'></i>",
			"<i class='fa fa-angle-left' aria-hidden='true'></i>"
			],
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},
				1000:{
					items:6
				}
			}
		};
		let options2 = {
            loop:true,
            margin:10,
            nav:false,
            autoplay : true,
			autoplayHoverPause : false,
			smartSpeed : 500,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1600:{
                    items:5
                },
                1920:{
                    items:6
                }
            }
        };
        let options3 = {
            loop:true,
            margin:10,
            nav:true,
            navText: ["<i class='fa fa-angle-right' aria-hidden='true'></i>","<i class='fa fa-angle-left' aria-hidden='true'></i>"],
	      	responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
        };

       jQuery('.company-item').click((event) => {
       		this.findCompanyOffer(event.target.parentNode.id);
       });

    //    jQuery('#formsearchtravel .select').select2({
		  // minimumResultsForSearch: Infinity
    //    });



	}

	openRegister(){
		this.registerOpen = !this.registerOpen;
		this.loginOpen=false;
	}

	openLogin(){
		this.loginOpen = !this.loginOpen;
		this.registerOpen=false;
	}
	

  	getOptions(departureDate){
  		 return	{
	  		 	responsive:{
		                    0:{
		                        items:1
		                    },
		                    375:{
		                        items:departureDate.length < 2 ? departureDate.length : 2
		                    },
		                    768:{
		                        items:departureDate.length < 3 ? departureDate.length : 3
		                    },
		                    1000:{
		                        items: departureDate.length < 4 ? departureDate.length : 4
		                    },
		                    1320:{
		                        items: departureDate.length < 5 ? departureDate.length : 5
		                    },
		                    1650:{
		                        items: departureDate.length < 6 ? departureDate.length : 6
		                    }
		                }
	            };
  	}



	goToNextOffer(offerType: string = "") {
		this._propertyService.findOfferByType(offerType).then(response => {
			this.nextOfferType = this.offerTypes[Math.floor(Math.random() * this.offerTypes.length)];
			this.currentOffer = response;
		});
	}

	findCompanyOffer(id: any = 0) {
		console.log("Find offers for specific company");
		this._propertyService.findOffersByCompany(id).then(response => {
			this.companyOffer = response;
		});
	}

	viewReview(id){
		console.log("view review with ID: " + id);
	}

	openArticle(id: number){
		console.log("Open article with ID: " + id);
	}

	onLoginSubmit(form){
		console.log(form.value);
	}

	onRegisterSubmit(form){
		console.log(form.value);
	}
	
	onSubmit(form) { 
		var searchvalues = {};
		// jQuery('#formsearchtravel select').each((i,element)=>{
		// 	searchvalues[element.name] = element.value;
		// });
		console.log(searchvalues);
	}

	updateMessage(m): void {
		console.log(m);
	}
}
