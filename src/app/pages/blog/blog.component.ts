import { Component, OnInit } from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  singleNews: any;
  loader:boolean = false;

	constructor(private _propertyService: PropertyService) { 
    this.loader = true;
		this._propertyService.getBlogList().then(response => {
			this.singleNews = response;
      this.loader = false;
		});
	}

  ngOnInit() {
  }

}
