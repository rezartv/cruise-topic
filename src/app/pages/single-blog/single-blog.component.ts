import { Component, OnInit } from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';

// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';


@Component({
  selector: 'app-single-blog',
  templateUrl: './single-blog.component.html',
  styleUrls: ['./single-blog.component.css']
})
export class SingleBlogComponent implements OnInit {

    singleBlog: any;
    loader:boolean = false;

	constructor(private _propertyService: PropertyService) { 
    this.loader = true;
		this._propertyService.getSingleBlog().then(response => {
			this.singleBlog = response;
      this.loader = false;
		});
	}

  ngOnInit() {
  }

}
