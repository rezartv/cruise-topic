import { Component, OnInit } from '@angular/core';
import { InnerHeaderLeftComponent } from './../../inner-header-left/inner-header-left.component';
import { LinkBarComponent } from './../../link-bar/link-bar.component';
// models
import { Search } from './../../models/search'; // model
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';

@Component({
  selector: 'app-last-min-offer',
  templateUrl: './last-min-offer.component.html',
  styleUrls: ['./last-min-offer.component.css']
})

export class LastMinOfferComponent implements OnInit {
// search form
search = new Search('ALL', 'ALL', 'ALL', 'ALL');

  loader:boolean = false;
   offerBlock: any;

  loginOpen = false;
  registerOpen=false;

  constructor(private _propertyService: PropertyService) { 
    
    this.loader = true;
		this._propertyService.putThreeBlockOffer().then(response => {
			this.offerBlock = response;
      this.loader = false;
		});
	}
  
  openRegister(){
    this.registerOpen = !this.registerOpen;
    this.loginOpen=false;
  }

  openLogin(){
    this.loginOpen = !this.loginOpen;
    this.registerOpen=false;
  }
  onLoginSubmit(form){
    console.log(form.value);
  }

  onRegisterSubmit(form){
    console.log(form.value);
  }
  

  ngOnInit() {
  }
  onSubmit(form)
  {
    
  }
}
