import { Component, OnInit } from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';

// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';

@Component({
  selector: 'app-ship-review',
  templateUrl: './ship-review.component.html',
  styleUrls: ['./ship-review.component.css']
})
export class ShipReviewComponent implements OnInit {

    shipreviews: any;
    loader:boolean = false;

	constructor(private _propertyService: PropertyService) { 
      this.loader = true;
			this._propertyService.getShipReview().then(response => {
			this.shipreviews = response;
      this.loader = false;
		});
	}

  ngOnInit() {
  }

}
