import { Component , EventEmitter, Output} from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-preventive-after',
  templateUrl: './preventive-after.component.html',
  styleUrls: ['./../search-result/search-result.component.css','./preventive-after.component.css']
})
export class PreventiveAfterComponent{

  onSubmit = false;
  onCarta = false;
  onBonifico = false;
  onBolletino = false;
  loader:boolean = false;

  detailsPreventiv: any;
	constructor(private _propertyService: PropertyService , private route : ActivatedRoute ) { 
    this.route.params.subscribe(params => {
  		this._propertyService.getPreventiveDetails(params).then(response => {
        this.loader = true;
  			this.detailsPreventiv = response;
        this.loader = false;
  		});
    });
	}

}
