import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { InnerHeaderLeftComponent } from './../../inner-header-left/inner-header-left.component';
import { LinkBarComponent } from './../../link-bar/link-bar.component';
// models
import { Search } from './../../models/search'; // model

// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';
declare var jQuery: any;

@Component({
	selector: 'app-offerte',
	templateUrl: './offerte.component.html',
	styleUrls: ['./offerte.component.css']
})
export class OfferteComponent { 
	// search form
	search = new Search('ALL', 'ALL', 'ALL', 'ALL');
	destination : string;
	title: string = 'Offer Categories';
	body:  string = 'This is where you chose a categorie';
	offerCategories: any;
	loader:boolean = false;

	loginOpen = false;
	registerOpen=false;
	constructor(public _propertyService: PropertyService , private route : ActivatedRoute ) { 
		this.route.params.subscribe(params => {
			this.loader = true;
			this._propertyService.getOfferCategories(params).then(response => {
			this.offerCategories = response;
			console.log(response);
			this.loader = false;
			});
		});		
	}

	ngOnInit() {
	    
       jQuery('select').select2({
		  minimumResultsForSearch: Infinity
       });
	}
	
	openRegister(){
		this.registerOpen = !this.registerOpen;
		this.loginOpen=false;
	}

	openLogin(){
		this.loginOpen = !this.loginOpen;
		this.registerOpen=false;
	}
	onLoginSubmit(form){
		console.log(form.value);
	}

	onRegisterSubmit(form){
		console.log(form.value);
	}
	

	updateMessage(m: string): void {
	    
	}
	onSubmit(form)
	{
		
	}
	
}
