import { Component, OnInit } from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';
import { RedWishlistBannerComponent  } from './../../red-wishlist-banner/red-wishlist-banner.component';
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';

@Component({
  selector: 'app-make-preventiv',
  templateUrl: './make-preventiv.component.html',
  styleUrls: ['./../search-result/search-result.component.css','./../preventive-details/preventive-details.component.css','./make-preventiv.component.css']
})
export class MakePreventivComponent implements OnInit {

    loader:boolean = false;

    lat: number = 41.9028;
    lng: number = 12.4964;
    zoom: number = 4;
    styles : any = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#3c4b68"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#27364b"},{"visibility":"on"}]}];
    markers = [
    {
      lat: 41.9028,
      lng: 12.4964,
     }];
     snazzy_info = [
     {
       content:"Content1"
     }];

  	offerPreventiv: any;
    options = {
                loop:true,
                nav:true,
                responsive:{
                          0:{
                              items:1
                          },
                          600:{
                              items:2
                          },
                          1000:{
                              items:3
                          }
                      }
            };
	constructor(private _propertyService: PropertyService) { 
    this.loader = true;
		this._propertyService.setOfferPreventiv().then(response => {
			this.offerPreventiv = response;
      this.loader = false;
		});
	}

  ngOnInit() {
  }
  
   mapClicked(event) {
      this.markers.push({
          lat: event.coords.lat,
          lng: event.coords.lng
      });
      this.snazzy_info.push({
       content:'<div style="width : 200px;"><div class="iw-title" style="text-align: center; font-size: 16px; font-weight: bold; padding-bottom: 5px;"><a href="single-port/80" target="_blank">BARCELLONA</a></div><div class="iw-content"><a href="single-port/80" target="_blank"><img src="https://cruisetopic.net/data_admin/ports_img/Barcellona-2.jpg" alt="BARCELLONA" height="100" width="100%"></a></div></div>'
     });
    }
}
