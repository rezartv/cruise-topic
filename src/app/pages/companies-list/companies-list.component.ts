import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { InnerHeaderLeftComponent } from './../../inner-header-left/inner-header-left.component';
import { LinkBarComponent } from './../../link-bar/link-bar.component';
// models
import { Search } from './../../models/search'; // model
declare var jQuery: any;

@Component({
  selector: 'app-companies-list',
  templateUrl: './companies-list.component.html',
  styleUrls: ['./companies-list.component.css']
})
export class CompaniesListComponent implements OnInit {
	// search form
	search = new Search('ALL', 'ALL', 'ALL', 'ALL');
	loginOpen = false;
	registerOpen=false;
  constructor() { }

  ngOnInit() { 
  	jQuery('select').select2({
		  minimumResultsForSearch: Infinity
       });
  }
	onSubmit(form)
	{
		
	}

	openRegister(){
		this.registerOpen = !this.registerOpen;
		this.loginOpen=false;
		jQuery('select').select2({
		  minimumResultsForSearch: Infinity
       });
	}

	openLogin(){
		this.loginOpen = !this.loginOpen;
		this.registerOpen=false;
		jQuery('select').select2({
		  minimumResultsForSearch: Infinity
       });
	}
	onLoginSubmit(form){
		console.log(form.value);
	}

	onRegisterSubmit(form){
		console.log(form.value);
	}
	

}
