import { Component, OnInit } from '@angular/core';
import { DestinationsHeadComponent } from './../../destinations-head/destinations-head.component';
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';

@Component({
  selector: 'app-destination-list',
  templateUrl: './destination-list.component.html',
  styleUrls: ['./destination-list.component.css']
})
export class DestinationListComponent implements OnInit {

  offerBlock: any;
  loader:boolean = false;

	constructor(private _propertyService: PropertyService) { 
    
    this.loader = true;
		this._propertyService.putThreeBlockOffer().then(response => {
			this.offerBlock = response;
      this.loader = false;
		});
	}

  ngOnInit() {
  }

}
