import { Component, OnInit } from '@angular/core';
import { InnerHeaderLeftComponent } from './../../inner-header-left/inner-header-left.component';
import { LinkBarComponent } from './../../link-bar/link-bar.component';
import { ActivatedRoute } from '@angular/router';

// models
import { Search } from './../../models/search'; // model
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';

@Component({
  selector: 'app-single-destination',
  templateUrl: './single-destination.component.html',
  styleUrls: ['./single-destination.component.css']
})
export class SingleDestinationComponent {
	// search form
	search = new Search('ALL', 'ALL', 'ALL', 'ALL');
	lat: number = 41.9028;
  	lng: number = 12.4964;
  	zoom: number = 4;
  	styles : any = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#3c4b68"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#27364b"},{"visibility":"on"}]}];
  	 markers = [
	  {
		  lat: 41.9028,
		  lng: 12.4964
	  }
  	]
	loader:boolean = false;
  	offerBlock: any;
  	loginOpen = false;
	registerOpen=false;
	constructor(private _propertyService: PropertyService , private route : ActivatedRoute  ) { 
		this.loader = true;
		this.route.params.subscribe(params => {
			this._propertyService.putThreeBlockOffer(params).then(response => {
				this.offerBlock = response;
				this.loader = false;
			});
		});
	}
	
	openRegister(){
		this.registerOpen = !this.registerOpen;
		this.loginOpen=false;
	}

	openLogin(){
		this.loginOpen = !this.loginOpen;
		this.registerOpen=false;
	}
	onLoginSubmit(form){
		console.log(form.value);
	}

	onRegisterSubmit(form){
		console.log(form.value);
	}
	

	mapClicked(event) {
		this.markers.push({
		    lat: event.coords.lat,
		    lng: event.coords.lng
		});

  }
	onSubmit(form)
	{
		
	}
}
