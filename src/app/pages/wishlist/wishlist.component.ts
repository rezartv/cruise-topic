import { Component, OnInit } from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';
import { SidebarSearchComponent } from './../../sidebar-search/sidebar-search.component';
import { WishlistSingleSearchComponent  } from './../../wishlist-single-search/wishlist-single-search.component';
import { RedWishlistBannerComponent  } from './../../red-wishlist-banner/red-wishlist-banner.component';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css','./../search-result/search-result.component.css']
})
export class WishlistComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
