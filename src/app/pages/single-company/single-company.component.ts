import { Component, OnInit } from '@angular/core';
import { InnerHeaderLeftComponent } from './../../inner-header-left/inner-header-left.component';
import { LinkBarComponent } from './../../link-bar/link-bar.component';
import { ActivatedRoute } from '@angular/router';

// models
import { Search } from './../../models/search'; // model
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';



@Component({
  selector: 'app-single-company',
  templateUrl: './single-company.component.html',
  styleUrls: ['./single-company.component.css']
})

export class SingleCompanyComponent {
	// search form
	search = new Search('ALL', 'ALL', 'ALL', 'ALL');
	
	loader:boolean = false;

	loginOpen = false;
	registerOpen=false;
	
  	singleCompanyPage: any;
	constructor(private _propertyService: PropertyService , private route : ActivatedRoute ) { 

		this.loader = true;
		this.route.params.subscribe(params => {
			this._propertyService.getSingleCompany(params).then(response => {
				this.singleCompanyPage = response;
				this.loader = false;
			});
		});
	}
	onSubmit(form)
	{
		
	}

	openRegister(){
		this.registerOpen = !this.registerOpen;
		this.loginOpen=false;
	}

	openLogin(){
		this.loginOpen = !this.loginOpen;
		this.registerOpen=false;
	}
	onLoginSubmit(form){
		console.log(form.value);
	}

	onRegisterSubmit(form){
		console.log(form.value);
	}
	

}
