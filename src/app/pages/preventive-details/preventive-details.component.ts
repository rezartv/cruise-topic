import { Component , EventEmitter, Output , ViewChild} from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';
// PROVIDERS
import { PropertyService } from './../../providers/property-service-mock';
import { ActivatedRoute } from '@angular/router';
import { AgmMap } from '@agm/core';

@Component({
  selector: 'app-preventive-details',
  templateUrl: './preventive-details.component.html',
  styleUrls: ['./../search-result/search-result.component.css','./preventive-details.component.css']
})
export class PreventiveDetailsComponent{

  @ViewChild(AgmMap) myMap: AgmMap;

  onSubmit = false;
  onCarta = false;
  onBonifico = false;
  onBolletino = false;
  loader:boolean = false;
  

  lat: number = 41.9028;
  lng: number = 12.4964;
  zoom: number = 4;
  styles : any = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#3c4b68"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#27364b"},{"visibility":"on"}]}];
  markers = [
  {
    lat: 41.9028,
    lng: 12.4964,
   }];
   snazzy_info = [
   {
     content:"Content1"
   }];

  detailsPreventiv: any;
	constructor(private _propertyService: PropertyService , private route : ActivatedRoute ) { 
    this.route.params.subscribe(params => {
  		this._propertyService.getPreventiveDetails(params).then(response => {
        this.loader = true;
  			this.detailsPreventiv = response;
        this.loader = false;
  		});
    });
	}



  @Output() clicked = new EventEmitter();
  onClicked(){
    this.onSubmit = !this.onSubmit; 
  }

  resize(){
    console.log('resize');
    this.myMap.triggerResize(true);
  }

   mapClicked(event) {
      this.markers.push({
          lat: event.coords.lat,
          lng: event.coords.lng
      });
      this.snazzy_info.push({
       content:'<div style="width : 200px;"><div class="iw-title" style="text-align: center; font-size: 16px; font-weight: bold; padding-bottom: 5px;"><a href="single-port/80" target="_blank">BARCELLONA</a></div><div class="iw-content"><a href="single-port/80" target="_blank"><img src="https://cruisetopic.net/data_admin/ports_img/Barcellona-2.jpg" alt="BARCELLONA" height="100" width="100%"></a></div></div>'
     });
    }
}
