import { Component, OnInit } from '@angular/core';
import { InnerHeaderCenterComponent } from './../../inner-header-center/inner-header-center.component';
import { SidebarSearchComponent } from './../../sidebar-search/sidebar-search.component';
import { OfferSingleSearchComponent } from './../../offer-single-search/offer-single-search.component';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
