import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule  } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { appRoutes } from './app.routes';

// all pages/Component of the page decleration
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { OfferteComponent } from './pages/offerte/offerte.component';
import { SearchResultComponent } from './pages/search-result/search-result.component';
import { PreventiveDetailsComponent } from './pages/preventive-details/preventive-details.component';
import { DestinationListComponent } from './pages/destination-list/destination-list.component';
import { CompaniesListComponent } from './pages/companies-list/companies-list.component';
import { SingleCompanyComponent } from './pages/single-company/single-company.component';
import { MakePreventivComponent } from './pages/make-preventiv/make-preventiv.component';
import { WishlistComponent } from './pages/wishlist/wishlist.component';
import { SingleDestinationComponent } from './pages/single-destination/single-destination.component';
import { SingleShipComponent } from './pages/single-ship/single-ship.component';
import { LastMinOfferComponent } from './pages/last-min-offer/last-min-offer.component';
import { BlogComponent } from './pages/blog/blog.component';
import { SingleBlogComponent } from './pages/single-blog/single-blog.component';
import { ShipReviewComponent } from './pages/ship-review/ship-review.component';
import { AddReviewComponent } from './pages/add-review/add-review.component';
import { PageFooterComponent } from './pages/page-footer/page-footer.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';
import { PreventiveAfterComponent } from './pages/preventive-after/preventive-after.component';

//Extra services
import {ToasterModule, ToasterService} from 'angular2-toaster';
import { CookieLawModule } from 'angular2-cookie-law';

// providers
import { PropertyService } from './providers/property-service-mock';

// pipes
import { PrettyPrintPipe, CapitalizePipe } from './pipes/prettyprint';

// components
import { InnerHeaderCenterComponent } from './inner-header-center/inner-header-center.component';
import { InnerHeaderLeftComponent } from './inner-header-left/inner-header-left.component';
import { LinkBarComponent } from './link-bar/link-bar.component';
import { SidebarSearchComponent } from './sidebar-search/sidebar-search.component';
import { OfferSingleSearchComponent } from './offer-single-search/offer-single-search.component';
import { WishlistSingleSearchComponent } from './wishlist-single-search/wishlist-single-search.component';
import { RedWishlistBannerComponent } from './red-wishlist-banner/red-wishlist-banner.component';
import { DestinationsHeadComponent } from './destinations-head/destinations-head.component';
import { AgmCoreModule } from '@agm/core';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { OwlModule } from 'ng2-owl-carousel';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SingleOfferComponent } from './pages/single-offer/single-offer.component';


@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		OfferteComponent,
		PrettyPrintPipe,
		CapitalizePipe,
		PreventiveDetailsComponent,
		InnerHeaderCenterComponent,
		InnerHeaderLeftComponent,
		LinkBarComponent,
		SearchResultComponent,
		SidebarSearchComponent,
		OfferSingleSearchComponent,
		MakePreventivComponent,
		WishlistComponent,
		WishlistSingleSearchComponent,
		RedWishlistBannerComponent,
		CompaniesListComponent,
		SingleCompanyComponent,
		DestinationsHeadComponent,
		DestinationListComponent,
		SingleDestinationComponent,
		SingleShipComponent,
		LastMinOfferComponent,
		BlogComponent,
		SingleBlogComponent,
		ShipReviewComponent,
		AddReviewComponent,
		PageFooterComponent,
		SubscribeComponent,
		PreventiveAfterComponent,
		LoginComponent,
		RegisterComponent,
		SingleOfferComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		JsonpModule,
		RouterModule.forRoot(appRoutes),
		OwlModule,
		AgmCoreModule.forRoot({
	      apiKey: 'AIzaSyCuWyzCLOSbgK3ZXCL_KqDC7aExefm_bJQ'
	    }),
	    AgmSnazzyInfoWindowModule,
	    BrowserAnimationsModule, 
	    ToasterModule,
	    CookieLawModule
	],
	providers: [
		PropertyService,
	],
	exports: [],
	bootstrap: [AppComponent]
})

export class AppModule { }
