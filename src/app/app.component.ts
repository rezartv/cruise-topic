import { Component } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToasterContainerComponent, ToasterService, ToasterConfig} from 'angular2-toaster';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],

})
export class AppComponent {

	isActive: boolean = false;
	toggleMenu(){
		this.isActive = this.isActive === true ? false : true;
	}
	public toasterconfig : ToasterConfig = 
        new ToasterConfig({
            tapToDismiss: false, 
            timeout: 3000
        });
}
