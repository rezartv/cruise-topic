import { Component, OnInit } from '@angular/core';
// PROVIDERS
import { PropertyService } from './../providers/property-service-mock';

@Component({
  selector: 'app-link-bar',
  templateUrl: './link-bar.component.html',
  styleUrls: ['./link-bar.component.css']
})
export class LinkBarComponent implements OnInit {
	  link_bar:any;
    loader:boolean = false;

  	constructor(private _propertyService: PropertyService)  { 
      this.loader = true;
  		this._propertyService.getLinkBar().then(response => {
			this.link_bar = response;
      this.loader = false;
		});
	}

  	ngOnInit() {
  	}

}
