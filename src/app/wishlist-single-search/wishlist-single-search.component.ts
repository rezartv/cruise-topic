import { Component, OnInit } from '@angular/core';
// PROVIDERS
import { PropertyService } from './../providers/property-service-mock';
declare var jQuery: any;
@Component({
  selector: 'app-wishlist-single-search',
  templateUrl: './wishlist-single-search.component.html',
  styleUrls: ['./wishlist-single-search.component.css','./../pages/search-result/search-result.component.css']
})
export class WishlistSingleSearchComponent implements OnInit {

  
 	singleOffers: any;
	loader:boolean = false;
	dataLoader : boolean = false;
	options = {
	            loop:true,
	            nav:true,
		      	responsive:{
	                    0:{
	                        items:1
	                    },
	                    600:{
	                        items:2
	                    },
	                    1000:{
	                        items:3
	                    }
	                }
	        };
	lat: number = 41.9028;
  	lng: number = 12.4964;
  	zoom: number = 4;
  	styles : any = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#3c4b68"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#27364b"},{"visibility":"on"}]}];
  	 markers = [
	  {
		  lat: 41.9028,
		  lng: 12.4964
	  }
  	]
	constructor(private _propertyService: PropertyService) { 
		this.dataLoader = true;
		this._propertyService.getWishlistResultOffer().then(response => {
			this.singleOffers = response;
			setTimeout(()=>{
				this.dataLoader = false;
			},3000);
		});
	}


  ngOnInit() {
  }


  	ngAfterViewInit() {
  		let that = this;
		jQuery('[data-emtoggle="emtooltip"]', '#page-top').hover(
			function() {
				console.log('test');
				var element = jQuery("<div class='emtooltip-content'></div>").html('<img class="emtooltip-img" src="'+jQuery(this).attr('data-emcontent')+'" alt="" />');
				jQuery( this ).append(element).find('div').fadeIn('slow');
			}, function() {
				console.log('test out');
				jQuery( this ).find(jQuery("div")).fadeOut( "slow", function() {
					jQuery(this).remove();
				});
			}
		);
 		var elements = jQuery('.cd-products-wrapper .owl-nav');
		for(var i = 0 ; i < elements.length ; i++)
		{
		    var el = elements[i]; 
		    elements[i].remove();
		    jQuery('.cd-products-wrapper')[i].append(el);
		}
  	}

  	getOptions(departureDate){
  		 return	{
	  		 	responsive:{
		                    0:{
		                        items:1
		                    },
		                    375:{
		                        items:departureDate.length < 2 ? departureDate.length : 2
		                    },
		                    768:{
		                        items:departureDate.length < 3 ? departureDate.length : 3
		                    },
		                    1000:{
		                        items: departureDate.length < 4 ? departureDate.length : 4
		                    },
		                    1320:{
		                        items: departureDate.length < 5 ? departureDate.length : 5
		                    },
		                    1650:{
		                        items: departureDate.length < 6 ? departureDate.length : 6
		                    }
		                }
	            };
  	}
}
