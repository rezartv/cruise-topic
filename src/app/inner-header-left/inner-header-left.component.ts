import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-inner-header-left',
  templateUrl: './inner-header-left.component.html',
  styleUrls: ['./inner-header-left.component.css']
})
export class InnerHeaderLeftComponent{

	@Input() page_title: string="no title";

  constructor() { }

  ngOnInit() {
  }

}
