/*!
 * FINAL LEK
 */
(function($) {

    //open interest point description
    $(document).on('click', '.cd-img-replace', function(){
        var selectedPoint = $(this).parent('li');
        if( selectedPoint.hasClass('is-open') ) {
            selectedPoint.removeClass('is-open').addClass('visited');
        } else {
            selectedPoint.addClass('is-open').siblings('.cd-single-point.is-open').removeClass('is-open').addClass('visited');
        }
    });
    //close interest point description
    $(document).on('click', '.cd-close-info' , function(event){
        event.preventDefault();
        $(this).parents('.cd-single-point').eq(0).removeClass('is-open').addClass('visited');
    });




    "use strict"; // Start of use strict


    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })


        // Make the rotation of the arrows on the accordion
    jQuery.fn.rotate = function(degrees) {
        $(this).css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
        '-moz-transform' : 'rotate('+ degrees +'deg)',
        '-ms-transform' : 'rotate('+ degrees +'deg)',
        'transform' : 'rotate('+ degrees +'deg)',
        '-webkit-transition': 'all 0.3s ease-out',
        '-moz-transition': 'all 0.3s ease-out',
        '-o-transition': 'all 0.3s ease-out',
        'transition': 'all 0.3s ease-out'});
    };

})(jQuery); // End of use strict
